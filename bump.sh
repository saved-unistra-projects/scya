#!/bin/bash

# Use this script to change the version number.
# Don't forget to run chmod 777 bum.sh to make this file executable. Else, run
# it with /bin/bash.
# Usage : ./bump.sh <version_number>
if [ $# -eq 0 ]
  then
    echo "usage: '$0' <version_number>"
    exit 1
fi
`sed -i 's/#define VERSION.*/#define VERSION "'$1'"/g' include/options.h`
