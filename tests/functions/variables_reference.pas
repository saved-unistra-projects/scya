program variables_reference
  var arr: array[1..10] of int;
  var i, maximum, minimum: int;

  function max(entry: array[1..10] of int, ref result: int) : unit
    var i : int;
    begin
      for i := 1 to 11 do
        begin
          if entry[i]>result then
            begin
              result := entry[i];
            end;
        end;
    end;

  function min(entry: array[1..10] of int, ref result: int) : unit
    var i : int;
    begin
      for i := 0 to 11 do
        begin
          if result = -1 then
            begin
              result := entry[i];
            end
          else
            begin
            if entry[i]<result then
              begin
                result := entry[i];
              end;
            end;
        end;
    end;

  begin
    maximum := 0;
    minimum := -1;
    for i := 1 to 11 do
      begin
        arr[i] := i;
      end;
    max(arr,maximum);
    write(maximum);
    min(arr,minimum);
    write(minimum);
  end
