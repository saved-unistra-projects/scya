program mix
var x, y, a, b, c, d, i, p: int;

  function factorielle(x: int) : int
    var i, res: int;
    begin
      res := 1;
      for i:= 1 to x+1 do
        begin
          res := res*i;
        end;
      return res;
    end;

  function sum(x: int) : int
    var i, res: int;
    begin
      i := 0;
      res := 0;
      while i<=x do
        begin
          res := res + i;
          i := i + 1;
        end;
      return res;
    end;

  function square(x: int) : int
    var res: int;
    begin
      res := x * x;
      return res;
    end;

  function cube(x: int) : int
    var res: int;
    begin
      res := square(x)*x;
      return res;
    end;

  function plus(x:int, y:int) : int
    var res: int;
    begin
      res := x+y;
      return res;
    end;

  function polynome(a:int, b:int, c:int, d:int, x:int) : int
    var y: int;
    begin
      y := a*cube(x) + b*square(x) + c*x + d;
      return y;
    end;

begin
  a := 4;
  b := 1;
  c := 3;
  d := 1;
  x := 5;
  p := polynome(a,b,c,d,x);
  y := factorielle(x) + sum(polynome(a,b,c,d,x));
  write factorielle(x);
  write sum(polynome(a,b,c,d,x));
  write y;
end
