program binary
var dec, bin, i: int;

  function modulo(x:int, y:int) : int
    var mod:int;
    begin
      mod := x - (x/y)*y;
      return mod;
    end;

  function tobinary(bn : int) : int
    var d, b, r, f : int;
    begin
      b := 0;
      f := 1;
      d := bn;
      while d > 0 do
        begin
          r := modulo(d,2);
          b := b + (r * f);
          f := f * 10;
          d := d/2;
        end;
      return b;
    end;

  begin
    for i := 0 to 10 do
      begin
        dec := i;
        bin := tobinary(i);
        write dec;
        write bin;
      end;
  end
