program recursive_bool
var x: int; 
var y: int;

  function fibbo(x: int) : int
    var res: int;
    var i: int;
    begin
      
      if x = 0 then
        begin
          return 0;
        end
      else
        begin
          if x = 1 then
            begin
              return 1;
            end
          else
            return fibbo(x - 1) + fibbo(x-2);
        end;
    end;

begin
  x := 0; 
  y := fibbo(x);
  write y;
  x := 1; 
  y := fibbo(x);
  write y;
  x := 3; 
  y := fibbo(x);
  write y;
  x := 20; 
  y := fibbo(x);
  write y;
end
