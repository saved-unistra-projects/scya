program polynome
  var a,b,c,d,x,y,i : int;

  function square(x: int) : int
    var res: int;
    begin
      res := x * x;
      return res;
    end;

  function cube(x: int) : int
    var res: int;
    begin
      res := square(x)*x;
      return res;
    end;

  function plus(x:int, y:int) : int
    var res: int;
    begin
      res := x+y;
      return res;
    end;

  function polynome(a:int, b:int, c:int, d:int, x:int) : int
    var y: int;
    begin
      y := a*cube(x) + b*square(x) + c*x + d;
      return y;
    end;

  begin
    a := 3;
    b := 10;
    c := 5;
    d := 1;
    for x := 0 to 10 do
      begin
        y := polynome(a,b,c,d,x);
        write y;
      end;
  end
