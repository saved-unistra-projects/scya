program arith
var x, y: int;

  function factorielle(x: int) : int
    var i, res: int;
    begin
      res := 1;
      for i:= 1 to x+1 do
        begin
          res := res*i;
        end;
      return res;
    end;

  function sum(x: int) : int
    var i, res: int;
    begin
      i := 0;
      res := 0;
      while i<=x do
        begin
          res := res + i;
          i := i + 1;
        end;
      return res;
    end;

  function modulo(x:int, y:int) : int
    var mod:int
    begin
      mod := x - (x/y)*y;
      return mod;
    end;

begin
  x := 4;
  y := factorielle(x) + sum(x) + modulo(x,2);
  write y;
end
