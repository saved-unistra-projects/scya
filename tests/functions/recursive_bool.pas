program recursive_bool
var x: int; 
var y: bool;

  function modulo(x:int, y:int) : int
    var mod:int
    begin
      mod := x - (x/y)*y;
      return mod;
    end;

  function is_even(x: int) : bool
    var res: bool;
    var i: int;
    begin
      i := modulo(x,2);
      if i = 0 then
        res := true
      else
        res := false;
      return res;
    end;

begin
  for x:= 0 to 10 do
    begin
      y := is_even(x);
      write y;
    end;
end
