program power_func
  var t1, nbr, puiss, max, min : int;

  function power(nb : int, pow : int) : int
    var res, it : int
    begin
      it := pow;
      res := 1;
      while it>0 do
        begin
          res := res * nb;
          it := it - 1;
        end;
      return res;
    end;

  function maximum(n1: int, n2: int) : int
    begin
      if n1>n2 then
        begin
          return n1;
        end
      else
        begin
          return n2;
        end;
    end;

  function minimum(n1: int, n2: int) : int
    begin
      if n1>n2 then
        begin
          return n2;
        end
      else
        begin
          return n1;
        end;
    end;

  begin
    t1 := 2;
    nbr := 10;
    puiss := 3;
    min := minimum(t1,nbr);
    max := maximum(nbr,puiss);
    write power(min,max);
  end
