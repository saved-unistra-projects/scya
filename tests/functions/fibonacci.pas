program fibonacci
var depart, resultat : int

  function fibonacci(x:int) : int
    var res: int;
    begin
      if x = 0 then
        begin
          res := 0;
        end
      else
        begin
          if x = 1 then
            begin
              res := 1;
            end
          else
            begin
              res := fibonacci(x-1) + fibonacci(x-2);
            end;
        end;
      return res;
    end;

begin
  for depart := 0 to 15 do
    begin
      resultat := fibonacci(depart);
      write resultat;
    end;
end
