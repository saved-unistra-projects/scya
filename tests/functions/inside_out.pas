program inside_out
var arr: array[1..10] of int;
var i, maximum: int;

function max(entry: array[1..10] of int) : int
  var i, res : int;
  begin
    res := 0;
    for i := 1 to 11 do
      begin
        if entry[i]>res then
          begin
            res := entry[i];
          end;
      end;
    return res;
  end;

function writeArray(entry: array[1..10] of int) : unit
  var i: int;
  begin
    for i := 1 to 11 do
      write entry[i];
  end;

function mutliplyBy2(ref entry: array[1..10] of int) : unit
  var i, maximum : int;
  begin
    maximum := max(entry);
    while maximum < 40 do
      begin
        for i := 1 to 11 do
          begin
            entry[i] := entry[i] * 2;
          end;
      maximum := max(entry);
      end;
    writeArray(entry);
  end;

begin
for i := 1 to 11 do
  begin
    arr[i] := i;
  end;
  writeArray(arr);
  mutliplyBy2(arr);
  writeArray(arr);
end
