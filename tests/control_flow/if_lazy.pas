program if_lazy
  var a, b:bool;
  begin
    b := false;
    a := true;
    if a and b then
        write 1
    else
        write 0;

    if b and a then
        write 1
    else
        write 0;

    if b or a then
        write 1
    else
        write 0;

    if a or b then
        write 1
    else
        write 0;

  end
