program if_lazy_function
var x, y, z: bool;

  function op_and(x:bool, y:bool) : bool
    var result:bool
    begin
      result := x and y;
      write result;
      return result;
    end;

  function op_or(x:bool, y:bool) : bool
    var result:bool
    begin
      result := x or y;
      write result;
      return result;
    end;

begin
  x := true;
  y := false;
  z := op_and(y,x);
  write z;
  z := op_or(y,x);
  write z;
end
