program premier_test
var parite, i, n: int;
var trouve: bool;
begin
  n := 89;
  trouve := false;
  parite := n - (n/2)*2;
  if parite = 0 then
    begin
      write 2;
    end
  else
    begin
      i := 3;
      while (i*i) <= n do
        begin
          if (n - (n/i)*i) = 0 then
            begin
              write i;
              i := n;
              trouve := true;
            end
          else
            begin
              i := i+2;
            end;
        end;
    end;
    begin
      if trouve = false then
        begin
          write 0;
        end;
    end;
end
