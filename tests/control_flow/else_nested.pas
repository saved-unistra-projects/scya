program else_ested_test
  var a, b:bool;
  begin
    a := false;
    b := true;
    if a = true then
      begin
        write 6;
        if b = true then
          write 8
        else
          write 9
      end
    else
      begin
        write 7;
        if b = true then
          write 4
        else
          write 5
      end;
  end
