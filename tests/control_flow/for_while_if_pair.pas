program for_while_if_pair_test
var i, j, b, it: int;
begin
  b := 10;
  it := 0;
  j := 0;
  for i:= 0 to 10 do
    begin
      while j<10 do
        begin
          if (it - (it/2)*2) = 0 then
            begin
              write it;
            end;
          it := it+1;
          j := j+1;
        end;
        j := 0;
    end;
end
