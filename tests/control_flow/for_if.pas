program for_if_test
var i, j, b: int;
var b1: bool;
begin
  b := 5;
  b1 := true;
  for i:= 0 to b do
    begin
      for j:= 0 to b do
        begin
          if b1 = true then
            begin
              write j;
              b1 := false;
            end
          else
            begin
              write -1;
              b1 := true;
            end;
        end;
    end;
end
