program modulo_test
var x, mod, tmp, res1, res2: int;
begin
  x := 1037;
  mod := 13;
  res1 := x - (x/mod) * mod;
  tmp := x;
  write res1;
  while tmp>=mod do
    begin
      tmp := tmp-mod;
    end;
  begin
    res2 := tmp;
    write res2;
  end;
  if res1 = res2 then
    begin
      write 1;
    end
  else
    begin
      write 0;
    end;
end
