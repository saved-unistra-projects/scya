program for_while_test
var i, j, b, it: int;
begin
  b := 10;
  it := 0;
  j := 0;
  for i:= 0 to 10 do
    begin
      while j<10 do
        begin
          write it;
          it := it+1;
          j := j+1;
        end;
        j := 0;
    end;
end
