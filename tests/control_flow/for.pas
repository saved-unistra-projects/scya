program for_test
var i, j, b: int;
begin
  b := 10;
  for i:= 0 to b do
    write(i);
  for i:= 0 to b do
    begin
      b := b-1;
      write(b);
    end;
  begin
    b := 10;
  end;
  for i:= 0 to b do
    begin
      write(i);
      write(b);
      b := b-1;
    end;
  for i:= 0 to 15 do
    begin
      for j := 0 to 15 do
        begin
          if i=j
            then
              write(j);
        end;
    end;
end
