program while_test
  var a,b,c: int;
  begin
    a := 0;
    while a<3 do 
    begin
      write a;
      a := a+1;
    end;
    while false do
    begin 
      write 0;
    end;
    a:=0;
    b:=0; 
    c:=3;
    while a<=3 do
    begin 
      while b<a do 
      begin
        write b;
        b:=b+1;
      end;
      a:=a+1;
      b:=0;
    end;
  end