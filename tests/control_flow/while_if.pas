program for_while_test
var i, j, res: int;
var b1: bool;
begin
  b1 := true;
  i := 1;
  while i<4 do
    begin
      j := 1;
      res := 1;
      while j<9 do
        begin
          res := res * i;
          if j=2 then
            begin
              write res;
            end
          else
            begin
              if j=4 then
                begin
                  write res;
                end
              else
                begin
                  if j=8 then
                    begin
                      write res;
                    end
                end;
            end;
          j := j+1;
        end;
      i := i+1;
    end;
end
