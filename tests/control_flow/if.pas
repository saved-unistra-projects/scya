program if_test
var nb1, nb2: int;
var b1, b2: bool;
begin
  nb1 := 1;
  nb2 := 2;
  b1 := true;
  b2 := false;
  if b1
  then
    write b1;
    write b2;
  if b2
  then 
  write nb2;
  if b1
  then
  begin
    if b2
    then
      write nb2;
    write nb1
  end;
  if b1 and b2
  then
    write nb2;
  if b2 or b1
  then
    write nb1;
  if nb1 = 1
  then
  begin
    nb1 := 2;
    if nb1 = 1
    then
      write nb2;
    if nb1 = 2
    then
      write 1;
    end
  end
