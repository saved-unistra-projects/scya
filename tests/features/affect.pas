program affect
  var a, b, c: int;
  begin
    a := 1;
    b := a;
    write b;
    c := 2;
    write b;
    a := c;
    write a;
  end
