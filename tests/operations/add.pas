program add
  var a, b, c: int;
  begin
    a := 1+2;
    write a;
    b := a+3;
    write b;
    b := 3+a;
    write b;
    c := a+b;
    write c;
    write 2+2;
  end