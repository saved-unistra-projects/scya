program mul
  var a, b, c: int;
  begin
    a := 2*3;
    write a;
    a := 3*2;
    write a;
    b := a*2;
    write b;
    c := a*b;
    write c;
    write 2*c;
    write 2*2;
  end