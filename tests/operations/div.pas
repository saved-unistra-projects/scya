program div
  var a, b, c: int;
  begin
    a := 5/2;
    write a;
    a := 12/3;
    write a;
    b := a/2;
    write b;
    c := a/b;
    write c;
    write 2/c;
    write 6/2;
  end