program uminus
  var apos, aneg: int;
  begin
    apos := 20;
    write apos;
    aneg := -apos;
    write aneg;
    write -apos;
    write apos * -20;
  end
