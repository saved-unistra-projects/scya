program sub
  var a, b, c: int;
  begin
    a := 2-1;
    write a;
    a := 1-3;
    write a;
    b := a-1;
    write b;
    c := a-b;
    write c;
    write 1-c;
    write 2-2;
  end