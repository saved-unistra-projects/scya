program power
  var a,b, res : int;
  begin
    a := 2;
    b := 10;
    res := a ^ b;
    write res;
    a := 3;
    b := 2;
    res := a ^ b;
    write res;
    b := 3;
    res := a ^ b;
    write res;
  end
