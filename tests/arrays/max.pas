program max
  var arr: array[1..20] of int;
  var i, maximum: int;

  function max(entry: array[1..20] of int) : int
    var i, res : int;
    begin
      res := 0;
      for i := 1 to 21 do
        begin
          if entry[i]>res then
            begin
              res := entry[i];
            end;
        end;
      return res;
    end;

  begin
    for i := 1 to 21 do
      begin
        arr[i] := i;
      end;
    maximum := max(arr);
    write(maximum);
  end
