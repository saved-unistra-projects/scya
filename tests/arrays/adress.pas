program adress
  var arr: array[1..10] of int;
  var i: int;

  function modification(entry: array[1..10] of int) : int
    var i : int;
    begin
      for i := 0 to 11 do
        begin
          entry[i] := i * 2;
        end;
      return 1;
    end;

  function modificationRef(ref entry: array[1..10] of int) : int
    var i : int;
    begin
      for i := 0 to 11 do
        begin
          entry[i] := i * 2;
        end;
      return 1;
    end;


  begin
    for i := 1 to 11 do
      begin
        arr[i] := i;
        write arr[i];
      end;
    modification(arr);
    for i := 1 to 11 do
      write arr[i];
    modificationRef(arr);
    for i := 1 to 11 do
      write arr[i];
  end
