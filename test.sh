#!/bin/bash

retval=0
if [ "$#" -eq 0 ]; then 
    args="tests"
else
    args=$@
fi

# Convert directories in subfiles
for arg in $args
do
    if [[ -d $arg ]]; then
        subfiles=$(find $arg -type f)
        for subfile in $subfiles
        do
            args+=" "$subfile
        done
    fi
done

# Diff for every compiled file
for arg in $args
do
    if [[ $arg =~ .\.pas ]]; then
        # Create the MIPS file
        ./bin/scalpa "$arg"

        if [[ $? -gt 0 ]]; then
        echo "$arg failed to compile"
          retval=$(($retval+1))
          continue
        fi

        # Compare the outputs and get the return value of diff
        expected_file=${arg%.*}.exp
        spim -file out.s | tail -n +6 | diff --color --label=$arg --label=$expected_file -uZ "$expected_file" -
        retval=$(($retval+$?))

        # Remove the MIPS file
        rm out.s
    fi
done


if [ "$retval" -eq 0 ]; then 
    echo -e "\e[32m====== EVERYTHING IS FINE ======\e[m"
    exit 0
fi
exit 1
