# SCYA

## Description
This is a compiler for a subset of PASCAL.

The compiler outputs MIPS code.

## Usage
```
Usage: scalpa [OPTION]... FILE
```

## Available options
```
  --version            Print version info and authors

  -t, --tos            Print the table of symbols

  -i, --intcode        Print the generated intermediate code

  -h, --help           Print this help and exit

  -o, --output=PATH    Output the resulting assembly code in the file pointed by PATH
```

## Output

The generated MIPS assembly is written in the file `out.s`, unless you specify a file path with the `-o` option.

## Running the code

In order to run the generated code, you can use a MIPS emulator like [SPIM](http://spimsimulator.sourceforge.net/) or [MARS](http://courses.missouristate.edu/kenvollmar/mars/).

## Building from source

### Dependencies

The following programs are required in order to successfully build SCYA :

- `flex` in order to generate the lexer.
- `yacc`/`bison` in order to generate the parser.
- `gcc`/`clang` in order to build the project. (If you use clang, change the `CC` variable in the makefile).

### Commands

This project contains a `Makefile`. In order to compile the project, please make you sure `GNU Make` is installed on your system.

Then run :

- `make` to compile the project. The resulting binary will be in the `bin` directory.
- `make run` to run compile and run the project in the same command
- `make clean` to clean binary files. This is sometimes needed for correct compilation.
- `make install` to install the executable to `/usr/local/bin`. This might require admin privilege.

You can use the following options with `make` :

- `LFLAGS` contains options for the flex command, needed to generate the lexer.
  - Exemple : `make LFLAGS='-d'` will build the project with debug output for flex.

- `CFLAGS` contains flags options for compilation.
  - Exemple : `make CFLAGS='-Werror'` will enable the `Werror` flags for gcc.
