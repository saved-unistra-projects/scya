#include <stdlib.h>

#include "tr_mips.h"
// Just to check the types (arrays)
#include "parser.h"
// TODO Check for error on all fprintfs

// Local structure to differenciate between local, global and temp variables.
struct var {
  int type;
  unsigned int tmp;
};

Stack *stack_from_st(SymTable *st) {
  Stack *s;
  if ((s = malloc(sizeof(Stack))) == NULL)
    return NULL;
  s->size = st->index;
  if (s->size)
    s->offset = st->table1D[0]->tmp;
  else
    s->offset = 0;
  if ((s->stack_pos = malloc(s->size * sizeof(unsigned int))) == NULL) {
    free(s);
    return NULL;
  }
  unsigned int pos_from_sp = 0;
  for (size_t i = 0; i < st->index; i++) {
    s->stack_pos[i] = pos_from_sp;
    // Compute size of array
    if (st->table1D[i]->type == SCA_ARRAY && !st->table1D[i]->by_ref) {
      unsigned long total_size = 1;
      for (unsigned int j = 0; j < st->table1D[i]->arr.dim; j++) {
        total_size *= st->table1D[i]->arr.dims[j].size;
      }
      pos_from_sp += total_size;
    } else {
      pos_from_sp++;
    }
  }
  s->total_stack_size = pos_from_sp;
  s->st = st;
  return s;
}

const char *get_global_name(SymTable *gst, unsigned int tmp) {
  for (size_t i = 0; i < gst->index; i++) {
    if (gst->table1D[i]->tmp == tmp) {
      return gst->table1D[i]->name;
    }
  }
  return NULL;
}

int tr_prg(Program *prg, FILE *out) {
  fprintf(out, ".data\n");
  tr_gst(prg->gst, out);
  fprintf(out, "true: .asciiz \"true\"\nfalse: .asciiz \"false\"\n");
  fprintf(out, ".text\n");
  fprintf(out, ".globl main\n");
  fprintf(out, "main:\n");
  tr_main_fun(prg, out);
  tr_gst_fun(prg->gst, out);
  return 0;
}

int tr_gst(SymTable *gst, FILE *out) {
  for (int i = 0; i < gst->size; i++) {
    Entry *ptr = gst->table[i];
    while (ptr != NULL) {
      if (ptr->type == SCA_ARRAY) {
        unsigned long full_size = 1;
        for (unsigned int j = 0; j < ptr->arr.dim; j++) {
          full_size *= ptr->arr.dims[j].size;
        }
        fprintf(out, "_%s_: .space %lu\n", ptr->name, full_size * 4);
      } else if (ptr->type == SCA_FUN) {
        ptr = ptr->next;
        continue;
      } else if (ptr->type == SCA_STRING_CONST)
        fprintf(out, "S%u: .asciiz %s\n", ptr->tmp, ptr->name);
      else
        fprintf(out, "_%s_: .word 0\n", ptr->name);
      ptr = ptr->next;
    }
  }
  return 0;
}

int tr_gst_fun(SymTable *gst, FILE *out) {
  for (int i = 0; i < gst->size; i++) {
    Entry *ptr = gst->table[i];
    while (ptr != NULL) {
      if (ptr->type != SCA_FUN) {
        ptr = ptr->next;
        continue;
      }
      fprintf(out, "_%s_:\n", ptr->name);
      tr_fun(&ptr->func, gst, out);
      ptr = ptr->next;
    }
  }
  return 0;
}

int tr_main_fun(Program *prg, FILE *out) {
  Quad *q = prg->code;
  Stack s = (Stack){.size = 0, .offset = prg->gst->index + 1};
  while (q != NULL) {
    tr_quad(q, &s, prg->gst, out);
    q = q->next;
  }
  fprintf(out, "li $v0 10\nsyscall\n");
  return 0;
}

int tr_fun(struct func *fun, SymTable *gst, FILE *out) {
  Quad *q = fun->code;
  Stack *s = stack_from_st(fun->st);
  while (q != NULL) {
    tr_quad(q, s, gst, out);
    q = q->next;
  }
  stack_free(s);
  return 0;
}

int tr_quad(Quad *q, Stack *s, SymTable *gst, FILE *out) {
  struct var vx, vy, vres;
  FILL_VAR(vx, q->x);
  FILL_VAR(vy, q->y);
  FILL_VAR(vres, q->res);
  switch (q->op) {
  case OP_NOOP:
    return 0;
    break;
  case OP_PARAM: {
    // res = by ref ?
    Entry f = st_fetch_from_tmp(s->st ? s->st : gst, vx.tmp);
    if (f.type == SCA_ARRAY && !vres.tmp) {
      // Passing array by value
      LOAD_ARRAY("$t1", vx);
      unsigned int total_size = get_total_array_size(f.arr);
      // Loop through array and push it onto the stack.
      unsigned int tmp = new_label();
      fprintf(out, "li $t0, %u\n", total_size);
      fprintf(out, "addiu $t1, $t1, %u\n", total_size * 4 - 4);
      fprintf(out, "L%u:\n", tmp);
      fprintf(out, "lw $t2, 0($t1)\n");
      PUSH("$t2");
      fprintf(out, "addiu $t1, $t1, %d\n", -4);
      fprintf(out, "addiu $t0, $t0, -1\n");
      fprintf(out, "bnez $t0, L%u\n", tmp);
    } else {
      if (vres.tmp) {
        if (f.name != NULL)
          // Temporary variable by ref
          LOAD_ADDR("$t1", vx);
        else
          // Give adress of the stack
          fprintf(out, "move $t1, $sp\n");
      } else
        LOAD("$t1", vx);
      PUSH("$t1");
    }
  } break;
  case OP_PRE_CALL:
    CTX_STORE();
    break;
  case OP_CALL:
    // Move stack pointer to avoid hitting local variables with temporary ones
    MOVE_STACK(vx);
    COMPUTE_FP(vx);
    fprintf(out, "jal _%s_\n", get_global_name(gst, vx.tmp));
    CTX_LOAD();
    if (vres.tmp != 0) {
      fprintf(out, "move $t2, $v0\n");
      STORE("$t2", vres);
    }
    break;
  case OP_RETURN:
    if (vx.tmp != 0) {
      LOAD("$v0", vx);
      fprintf(out, "jr $ra\n");
    } else
      fprintf(out, "j $ra\n");
    break;
  case OP_READ_ARR:
    LOAD("$t1", vy);
    LOAD_ARRAY("$t0", vx);
    fprintf(out, "sll $t1, $t1, 2\n"); // Multiply by 4
                                       /* if (vx.type == ISLOCAL)
                                          fprintf(out, "neg $t1, $t1\n");*/
    fprintf(out, "add $t1, $t1, $t0\n");
    fprintf(out, "lw $t2, 0($t1)\n");
    STORE("$t2", vres);
    break;
  case OP_WRITE_ARR:
    LOAD("$t1", vy);
    LOAD("$t0", vx);
    LOAD_ARRAY("$t2", vres);
    /*    if (vx.type == ISLOCAL)
          fprintf(out, "neg $t1, $t1\n");*/
    fprintf(out, "sll $t1, $t1, 2\n"); // Multiply by 4
    fprintf(out, "add $t2, $t2, $t1\n");
    fprintf(out, "sw $t0, 0($t2)\n");
    break;
  case OP_LABEL:
    fprintf(out, "L%u:\n", q->x);
    break;
  case OP_IF_NOT_GOTO:
    LOAD("$t2", vx);
    fprintf(out, "beqz $t2, L%u\n", q->y);
    break;
  case OP_IF_GOTO:
    LOAD("$t2", vx);
    fprintf(out, "bgtz $t2, L%u\n", q->y);
    break;
  case OP_GOTO:
    fprintf(out, "j L%u\n", q->x);
    break;
  case OP_AFFECT:
    LOAD("$t2", vx);
    STORE("$t2", vres);
    break;
  case OP_AFFECTI:
    LOAD_IM("$t2", q->x);
    STORE("$t2", vres);
    break;
  case OP_NEG:
    LOAD("$t0", vx);
    fprintf(out, "neg $t2, $t0\n");
    STORE("$t2", vres);
    break;
  case OP_MULT:
    BINOP("mul");
    break;
  case OP_PLUS:
    BINOP("add");
    break;
  case OP_DIV:
    BINOP("div");
    break;
  case OP_SUB:
    BINOP("sub");
    break;
  case OP_AND:
    BINOP("and");
    break;
  case OP_OR:
    BINOP("or");
    break;
  case OP_XOR:
    BINOP("xor");
    break;
  case OP_EQ:
    BINOP("seq");
    break;
  case OP_NE:
    BINOP("sne");
    break;
  case OP_LT:
    BINOP("slt");
    break;
  case OP_GT:
    BINOP("sgt");
    break;
  case OP_LE:
    BINOP("sle");
    break;
  case OP_GE:
    BINOP("sge");
    break;
  case OP_WRITE_INT:
    LOAD("$a0", vx);
    WRITE_INT();
    NEWLINE();
    break;
  case OP_WRITE_BOOL:
    LOAD("$t0", vx);
    unsigned int if_false = new_label();
    unsigned int if_true = new_label();
    fprintf(out, "beqz $t0, L%u\n", if_false);
    WRITE_TRUE();
    fprintf(out, "j L%u\nL%u:\n", if_true, if_false);
    WRITE_FALSE();
    fprintf(out, "L%u:\n", if_true);
    NEWLINE();
    break;
  case OP_WRITE_STRING:
    fprintf(out, "li $v0, 4\n");
    fprintf(out, "la $a0, S%u\n", vx.tmp);
    fprintf(out, "syscall\n");
    break;
  case OP_READ:
    fprintf(out, "li $v0, 5\nsyscall\n");
    STORE("$v0", vx);
    break;
  case OP_POW: {
    unsigned int label1 = new_label();
    unsigned int label2 = new_label();
    LOAD("$t1", vy);
    LOAD("$t0", vx);
    fprintf(out, "li $t2, 1\n");
    fprintf(out, "li $t3, 0\n");
    fprintf(out, "L%u:\n", label1);
    fprintf(out, "beq $t3, $t1, L%u\n", label2);
    fprintf(out, "mul $t2, $t0, $t2\n");
    fprintf(out, "addi $t3, $t3, 1\n");
    fprintf(out, "j L%u\n", label1);
    fprintf(out, "L%u:\n", label2);
    STORE("$t2", vres);
    break;
  }
  }
  return 0;
}

Stack *stack_new() {
  Stack *s;
  if ((s = malloc(sizeof(Stack))) == NULL)
    return NULL;
  s->size = 0;
  return s;
}

void stack_free(Stack *s) {
  free(s->stack_pos);
  free(s);
}
