#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "parser.h"
#include "symtable.h"
#include "utils.h"

int hash(char *str, int size) {
  unsigned int hash = 5381;
  int c;
  unsigned char *s = (unsigned char *)str;

  while ((c = *s++))
    hash = ((hash << 5) + hash) + c; /* hash * 33 + c */

  return abs((int)hash) % size;
}

SymTable *st_new() {
  SymTable *st;
  if ((st = malloc(sizeof(SymTable))) == NULL)
    return NULL;
  st->size = MAX_SIZE;
  if ((st->table = calloc(MAX_SIZE, sizeof(Entry *))) == NULL) {
    free(st);
    return NULL;
  }
  st->index = 0;
  st->size1D = MAX_SIZE;
  if ((st->table1D = calloc(MAX_SIZE, sizeof(Entry *))) == NULL) {
    free(st->table);
    free(st);
    return NULL;
  }
  return st;
}

void st_add(SymTable *st, char *name) {

  Entry *new = malloc(sizeof(Entry));
  new->type = -1;
  new->arr.type = -1;
  new->tmp = 0;
  new->name = malloc(strlen(name) + 1);
  new->next = NULL;
  TEST_ERROR_W_MSG(
      snprintf(new->name, MAX_STRING, "%s", name), >, MAX_STRING,
      "Fatal error : snprint failed, output witten Wrong in symtable\n");
  new->by_ref = 0;
  strcpy(new->name, name);

  if (st->index == st->size1D) {
    st->table1D = realloc(st->table1D, st->size1D * 2 * sizeof(Entry *));
    st->size1D = st->size1D * 2;
    resize_hash_table(st);
  }
  st->table1D[st->index] = new;
  st->index++;

  int index = hash(name, st->size1D);

  if (st->table[index] == NULL) {
    st->table[index] = new;
  } else {
    Entry *ptr = st->table[index];
    while (ptr->next != NULL) {
      ptr = ptr->next;
    }
    ptr->next = new;
  }
}

void resize_hash_table(SymTable *st) {
  free(st->table);
  st->size = st->size * 2;
  st->table = calloc(st->size, sizeof(Entry *));
  for (size_t i = 0; i < st->index; i++) {
    st->table1D[i]->next = NULL;
    int index = hash(st->table1D[i]->name, st->size1D);
    Entry *ptr = st->table[index];
    if (ptr == NULL) {
      st->table[index] = st->table1D[i];
      continue;
    }
    while (ptr->next != NULL) {
      ptr = ptr->next;
    }
    ptr->next = st->table1D[i];
  }
}

void st_set(SymTable *st, char *name, Entry e) {
  int index = hash(name, st->size1D);
  Entry *ptr = st->table[index];
  while (ptr != NULL) {
    if (strcmp(ptr->name, name) == 0) {
      *ptr = e;
      break;
    } else {
      ptr = ptr->next;
    }
  }
}

void st_set_tmp(SymTable *st, char *name, unsigned int tmp) {
  int index = hash(name, st->size1D);
  Entry *ptr = st->table[index];
  while (ptr != NULL) {
    if (strcmp(ptr->name, name) == 0) {
      ptr->tmp = tmp;
      break;
    } else {
      ptr = ptr->next;
    }
  }
}

void st_set_by_ref(SymTable *st, char *name, int ref) {
  Entry e = st_fetch(st, name);
  e.by_ref = ref;
  st_set(st, name, e);
}

size_t st_search(SymTable *st, char *name) {
  for (size_t i = 0; i < st->index; i++) {
    if (strcmp(st->table1D[i]->name, name) == 0)
      return i;
  }
  // Should not happen
  return 0;
}

int st_complete_type(SymTable *st, int type) {
  size_t i;
  for (i = 0; i < st->index; i++) {
    if (st->table1D[i]->type == -1) {
      if (type == SCA_ARRAY)
        st->table1D[i]->arr.type = -1;
      if (type == SCA_FUN)
        st->table1D[i]->func.ret_type = -1;
      st->table1D[i]->type = type;
    }
  }
  return 0;
}

int st_complete_array_type(SymTable *st, int type, unsigned int dim,
                           struct dim *dims) {
  size_t i;
  for (i = 0; i < st->index; i++) {
    if (st->table1D[i]->type == SCA_ARRAY && st->table1D[i]->arr.type == -1) {
      st->table1D[i]->arr.type = type;
      st->table1D[i]->arr.dim = dim;
      st->table1D[i]->arr.dims = malloc(dim * sizeof(struct dim));
      for (unsigned int j = 0; j < dim; j++) {
        st->table1D[i]->arr.dims[j] = dims[j];
      }
    }
  }
  return 0;
}

int st_complete_func_type(SymTable *st, int ret_type, unsigned int arity,
                          struct parameter *params, Quad *code, SymTable *lst) {
  size_t i;
  for (i = 0; i < st->index; i++) {
    if (st->table1D[i]->type == SCA_FUN &&
        st->table1D[i]->func.ret_type == -1) {
      st->table1D[i]->func.ret_type = ret_type;
      st->table1D[i]->func.arity = arity;
      st->table1D[i]->func.params = malloc(arity * sizeof(struct parameter));
      memcpy(st->table1D[i]->func.params, params,
             arity * sizeof(struct parameter));
      st->table1D[i]->func.code = code;
      st->table1D[i]->func.st = lst;
    }
  }
  return 0;
}

void st_complete_code(SymTable *st, char *name, Quad *code) {
  size_t index = st_search(st, name);
  st->table1D[index]->func.code = code;
}

Entry st_fetch(SymTable *st, char *name) {
  int index = hash(name, st->size1D);
  Entry *ptr = st->table[index];
  while (ptr != NULL) {
    if (strcmp(ptr->name, name) == 0) {
      return *ptr;
    }
    ptr = ptr->next;
  }
  return (Entry){.name = NULL, .tmp = 0, .type = -1};
}

Entry st_fetch1D(SymTable *st, int index) { return *st->table1D[index]; }

Entry st_fetch_from_tmp(SymTable *st, unsigned int tmp) {
  for (size_t i = 0; i < st->index; i++) {
    if (st->table1D[i]->tmp == tmp)
      return *st->table1D[i];
  }
  return (Entry){.name = NULL, .tmp = 0, .type = -1};
}

unsigned int get_total_array_size(struct arr arr) {
  unsigned int total_size = 1;
  for (unsigned int i = 0; i < arr.dim; i++) {
    total_size *= arr.dims[i].size;
  }
  return total_size;
}

void st_free(SymTable *st) {
  size_t i;
  for (i = 0; i < st->index; i++) {
    free(st->table1D[i]->name);
    if (st->table1D[i]->type == SCA_ARRAY) {
      free(st->table1D[i]->arr.dims);
    }
    if (st->table1D[i]->type == SCA_FUN) {
      q_free(st->table1D[i]->func.code);
      st_free(st->table1D[i]->func.st);
      free(st->table1D[i]->func.params);
    }
    free(st->table1D[i]);
  }
  free(st->table1D);
  free(st->table);
  free(st);
}

char *params2string(SymTable *gst, char *func_name) {
  Entry f = st_fetch(gst, func_name);
  char *buf = malloc(1);
  size_t buf_size = 1;
  char type_buf[10] = "";
  buf[0] = '\0';
  for (unsigned int i = 0; i < f.func.arity; i++) {
    char *name = f.func.st->table1D[i]->name;
    type2str(type_buf, f.func.st->table1D[i]->type);
    buf_size += strlen(name) + 4 + strlen(type_buf);
    if ((buf = realloc(buf, buf_size)) == NULL) {
      perror("Something went wrong while printing a function");
    }
    if (buf[0] != '\0')
      strncat(buf, ", ", buf_size);
    strncat(buf, name, buf_size);
    strncat(buf, ": ", buf_size);
    strncat(buf, type_buf, buf_size);
  }
  return buf;
}

void st_print(SymTable *st, int print_code) {
  st_print_from_func(st, NULL, 0);
  for (size_t i = 0; i < st->index; i++) {
    if (st->table1D[i]->type == SCA_FUN) {
      st_print_from_func(st, st->table1D[i]->name, print_code);
    }
  }
}

void st_print_from_func(SymTable *gst, char *func_name, int print_code) {
  SymTable *st;
  if (func_name != NULL) {
    st = st_fetch(gst, func_name).func.st;
    char *params = params2string(gst, func_name);
    char buf[5];
    type2str(buf, st_fetch(gst, func_name).func.ret_type);
    printf("Function %s (%s) -> %s\n", func_name, params, buf);
    free(params);
    if (print_code)
      q_listprint(st_fetch(gst, func_name).func.code);
  } else {
    st = gst;
  }
  printf("+================= Symbol Table =========================+\n");
  printf("|%-30s | %7s | %13s|\n", "id", "var num", "type");
  printf("|--------------------------------------------------------|\n");
  for (int i = 0; i < st->size; i++) {
    if (st->table[i] != NULL) {
      Entry *ptr = st->table[i];
      while (ptr != NULL) {
        printf("|%-30s | ", ptr->name);
        printf("%7u | ", ptr->tmp);
        const char *typename;
        switch (ptr->type) {
        case SCA_INT:
          typename = "int";
          break;
        case SCA_BOOL:
          typename = "bool";
          break;
        case SCA_UNIT:
          typename = "unit";
          break;
        case SCA_ARRAY:
          switch (ptr->arr.type) {
          case SCA_INT:
            typename = "array of int";
            break;
          case SCA_BOOL:
            typename = "array of bool";
            break;
          default:
            typename = "array of ???";
            break;
          }
          break;
        case SCA_FUN:
          typename = "function";
          break;
        default:
          typename = "???";
          break;
        }
        printf("%13s|\n", typename);
        ptr = ptr->next;
      }
    }
  }
  printf("+========================================================+\n");
}
