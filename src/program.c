#include <stdlib.h>

#include "program.h"

Program *prg_new() {
  Program *prg;
  if ((prg = malloc(sizeof(Program))) == NULL)
    return NULL;
  prg->code = NULL;
  prg->gst = NULL;
  prg->size = 0;
  prg->size_max = MAX_FUNC_SIZE;
  if ((prg->functions = calloc(MAX_FUNC_SIZE, sizeof(Function *))) == NULL) {
    free(prg);
    return NULL;
  }

  return prg;
}

void prg_set_code(Program *prg, Quad *code) { prg->code = code; }

Function *fun_new(SymTable *st, Quad *l) {
  Function *fun;
  if ((fun = malloc(sizeof(Function))) == NULL)
    return NULL;
  fun->st = st;
  fun->code = l;
  return fun;
}

void prg_set_gst(Program *prg, SymTable *gst) { prg->gst = gst; }

void prg_free(Program *prg) {
  size_t i;
  for (i = 0; i < prg->size; i++) {
    fun_free(prg->functions[i]);
  }
  free(prg->functions);
  if (prg->code)
    q_free(prg->code);
  if (prg->gst)
    st_free(prg->gst);
  free(prg);
}

void fun_free(Function *fun) {
  st_free(fun->st);
  q_free(fun->code);
  free(fun);
}
