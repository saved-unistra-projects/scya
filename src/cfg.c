#include "cfg.h"

CFG *create_cfg(Program *program) {
  CFG *cfg, *cfg_temp;
  unsigned int i;
  Entry **symtable = program->gst->table1D;

  // Main tree of the CFG (since the program always has a main)
  cfg = (CFG *)malloc(sizeof(CFG));
  cfg->name = "main";
  cfg->next = NULL;
  cfg->label = code_to_cft(program->code);

  // For each function, a separate tree in the CFG
  for (i = 0; i < program->gst->index; i++) {
    if (symtable[i]->type == SCA_FUN) {
      cfg_temp = cfg;
      cfg = (CFG *)malloc(sizeof(CFG));
      cfg->name = symtable[i]->name;
      cfg->next = cfg_temp;
      cfg->label = code_to_cft(symtable[i]->func.code);
    }
  }

  return cfg;
}

Block **code_to_cft(Quad *code) {
  Quad *code_ptr;
  Block *block, *block_ptr;
  Block **blocks_array;
  unsigned int id, length, max_label, new_block_flag;

  id = 0;
  length = 0;
  max_label = 0;
  new_block_flag = 0;

  // First block
  block = (Block *)malloc(sizeof(Block));
  block->id = id;
  block->label = 0;
  block->code = code;
  block->next = NULL;
  block->goto_branch_label = -1;
  block->cond_branch_label = -1;

  // Loop on quads init to build a "straight" CFG with each block connected
  // to the next
  block_ptr = block;
  code_ptr = code;
  while (code_ptr != NULL) {
    if (code_ptr->op == OP_LABEL || new_block_flag) {
      // End of the previous block
      block_ptr->length = length;
      length = 0;
      block_ptr->next = (Block *)malloc(sizeof(Block));

      // New block
      block_ptr = block_ptr->next;
      block_ptr->id = ++id;
      block_ptr->code = code_ptr;
      block_ptr->next = NULL;
      block_ptr->goto_branch_label = -1;
      block_ptr->cond_branch_label = -1;
      if (code_ptr->op == OP_LABEL) {
        max_label = code_ptr->x > max_label ? code_ptr->x : max_label;
        block_ptr->label = code_ptr->x;
      } else {
        block_ptr->label = -1;
      }

      new_block_flag = 0;
    }

    // If branching: end of the block
    // special case: block ending with goto -> not branched with the next one
    if (code_ptr->op == OP_GOTO) {
      new_block_flag = 1;
      block_ptr->goto_branch_label = code_ptr->x;
    } else if (code_ptr->op == OP_IF_GOTO || code_ptr->op == OP_IF_NOT_GOTO) {
      new_block_flag = 1;
      block_ptr->cond_branch_label = code_ptr->y;
    }

    length += 1;
    code_ptr = code_ptr->next;
  }
  // Completion of the last block
  block_ptr->length = length;
  block_ptr->goto_branch_label = -1;
  id += 1;

  // Using an array to store refs of the blocks with a label to
  // avoid harassing tree scanning
  blocks_array = (Block **)malloc((max_label + 1) * sizeof(Block *));
  max_label = 0;
  block_ptr = block;
  while (block_ptr != NULL) {
    if (block_ptr->label > -1) {
      blocks_array[block_ptr->label] = block_ptr;
    }
    block_ptr = block_ptr->next;
  }

  return blocks_array;
}

void free_cfg(CFG *cfg) {
  CFG *cfg_ptr;
  Block *block, *block_ptr;

  // For each tree of the CFG
  while (cfg != NULL) {

    // For each block of the tree
    block = cfg->label[0];
    while (block != NULL) {
      // Free the block
      block_ptr = block->next;
      free(block);
      block = block_ptr;
    }

    // Free the label array
    free(cfg->label);

    // Free the tree
    cfg_ptr = cfg->next;
    free(cfg);
    cfg = cfg_ptr;
  }
}

void export_cfg(CFG *cfg, const char *path) {
  FILE *dot_file;
  CFG *cfg_ptr;
  Block *block_ptr;

  // Beginning of the file
  dot_file = fopen(path, "w");
  TEST_ERROR(dot_file, ==, NULL);
  fprintf(dot_file, "digraph cfg {\n");

  // For each tree of the CFG, we make a subgraph
  cfg_ptr = cfg;
  while (cfg_ptr != NULL) {
    fprintf(dot_file, "\tsubgraph %s {\n", cfg_ptr->name);

    // For each block of the tree, we create a node
    block_ptr = cfg_ptr->label[0];
    while (block_ptr != NULL) {
      // The block

      // Its mandatory branching (goto/next)
      if (block_ptr->goto_branch_label > -1) {
        fprintf(dot_file, "\t\t%s_%d", cfg_ptr->name, block_ptr->id);
        fprintf(dot_file, " -> %s_%d;\n", cfg_ptr->name,
                cfg_ptr->label[block_ptr->goto_branch_label]->id);
      } else if (block_ptr->next != NULL) {
        fprintf(dot_file, "\t\t%s_%d", cfg_ptr->name, block_ptr->id);
        fprintf(dot_file, " -> %s_%d;\n", cfg_ptr->name, block_ptr->next->id);
      }
      // If last block (with no goto)
      else {
        fprintf(dot_file, "\t\t%s_%d;\n", cfg_ptr->name, block_ptr->id);
      }
      // Alternative branching (in case on IF_GOTO/IF_NOT_GOTO)
      if (block_ptr->cond_branch_label > -1) {
        fprintf(dot_file, "\t\t%s_%d", cfg_ptr->name, block_ptr->id);
        fprintf(dot_file, " -> %s_%d;\n", cfg_ptr->name,
                cfg_ptr->label[block_ptr->cond_branch_label]->id);
      }

      block_ptr = block_ptr->next;
    }

    // End of the subgraph
    fprintf(dot_file, "\t}\n");
    cfg_ptr = cfg_ptr->next;
  }

  // End of the graph
  fprintf(dot_file, "}\n");
  TEST_ERROR(fclose(dot_file), ==, EOF);
}