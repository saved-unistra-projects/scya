%define parse.error verbose
%{
#include <stdio.h>
#include <string.h>
#include <stddef.h>

#include "symtable.h"
#include "program.h"
#include "intercode.h"
#include "utils.h"

#define INIT_CODE(DEST) do {\
  DEST.code = malloc(sizeof(Quad *));\
  *DEST.code = q_new(OP_NOOP, 0, 0, 0);\
} while(0)

#define INIT_CODE_WITH(DEST, OP, X, Y, RES) do {\
  DEST.code = malloc(sizeof(Quad *));\
  *DEST.code = q_new(OP, X, Y, RES);\
} while(0)

#define COPY_CODE(DEST, SRC) do {\
  DEST.code = SRC.code;\
} while(0)

#define CONCAT_CODE(DEST, SRC) do {\
  q_concat(*DEST.code, *SRC.code);\
  free(SRC.code);\
} while(0)

#define GENCODE(DEST, OP, X, Y, RES) do {\
  q_concat(*DEST.code, q_new(OP, X, Y, RES));\
} while(0)

#define COMP(OP, RES, OP1, OP2) do {\
      RES.type = SCA_BOOL;\
      RES.tmp = new_tmp();\
      COPY_CODE(RES, OP1);\
      CONCAT_CODE(RES, OP2);\
      GENCODE(RES, OP, OP1.tmp, OP2.tmp, RES.tmp);\
} while(0)

#define BINOP(OP, RES, OP1, OP2) do {\
      RES.type = OP1.type;\
      RES.tmp = new_tmp();\
      COPY_CODE(RES, OP1);\
      CONCAT_CODE(RES, OP2);\
      GENCODE(RES, OP, OP1.tmp, OP2.tmp, RES.tmp);\
} while(0)

#define SET_LAST(SRC, POS, VAL) do {\
  get_last(*SRC.code)->POS = VAL;\
} while(0)


#define ERROR(STR, ...) do {\
  fprintf(stderr, "line %d: " STR, yylineno, __VA_ARGS__);\
} while(0)

#define COMPUTE_INDEX(SYM, ARR_SYM, ARR_ID, ACC) do {\
  unsigned int tmp = new_tmp();\
  unsigned int tmp2 = new_tmp();\
  CONCAT_CODE(SYM, ARR_SYM.list[0]);\
  GENCODE(SYM, OP_AFFECTI, ARR_ID.arr.dims[0].start_index, 0, ACC);\
  GENCODE(SYM, OP_SUB, ARR_SYM.list[0].tmp, ACC, ACC);\
  for (unsigned long i = 1; i < ARR_SYM.size; i++) {\
    CONCAT_CODE(SYM, ARR_SYM.list[i]);\
    GENCODE(SYM, OP_AFFECTI, ARR_ID.arr.dims[i].start_index, 0, tmp);\
    GENCODE(SYM, OP_SUB, ARR_SYM.list[i].tmp, tmp, tmp);\
    GENCODE(SYM, OP_AFFECTI, ARR_ID.arr.dims[i - 1].size, 0, tmp2);\
    GENCODE(SYM, OP_MULT, tmp, tmp2, tmp);\
    GENCODE(SYM, OP_PLUS, tmp, ACC, ACC);\
  }\
} while(0)


#define OP_ERROR(OP1, OP, OP2) do {\
  if (OP1.type != OP2.type) {\
    char buf[64];\
    char buf2[64];\
    type2str(buf, OP1.type);\
    type2str(buf2, OP2.type);\
    ERROR("type error: can't %s expressions of type %s and %s\n",\
           OP, buf, buf2);\
    yyfatal = 1;\
  }\
} while(0)

#define AFFECT_ERROR(OP1, OP2) do {\
  if (OP1.type != OP2.type &&\
     (OP1.type == SCA_ARRAY && OP1.arr.type != OP2.type)) {\
    char buf[64];\
    char buf2[64];\
    if (OP1.type == SCA_ARRAY)\
      type2str(buf, OP1.arr.type);\
    else\
      type2str(buf, OP1.type);\
    type2str(buf2, OP2.type);\
    ERROR("type error: trying to assign expression of type '%s' "\
               "to variable of type '%s'.\n", buf2, buf);\
    yyfatal = 1;\
  }\
} while(0)

// Must provide more arguments than needed to error because of __VA_ARGS__
#define COMP_ERROR(OP1) do {\
  if (OP1.type != SCA_INT) {\
    ERROR("type error: the '<', '<=', '>' and '>=' operators "\
          "can only be used for arithmetic expressions.%s\n", "");\
    yyfatal = 1;\
  }\
} while(0)

#define FETCH_ID(ST, GST, NAME) do {\
  id = st_fetch(ST, NAME);\
  if (id.name == NULL && id.type == -1) {\
    id = st_fetch(GST, NAME);\
    if (id.name == NULL && id.type == -1) {\
      ERROR("Undeclared identifier '%s'.\n", NAME);\
      yyfatal = 1;\
    }\
  }\
} while(0)

#define CALL_ERROR(ENT) do {\
  if (ENT.type != SCA_FUN) {\
    ERROR("trying to call %s, which is not a function\n", ENT.name);\
    yyfatal = 1;\
  }\
} while(0)

#define ARRAY_ERROR(ENT) do {\
  if (ENT.type != SCA_ARRAY) {\
    ERROR("trying to use %s as an array\n", ENT.name);\
    yyfatal = 1;\
  }\
} while(0)

#define YYCHECKFATAL(CONTEXT) do {\
  if (yyfatal) {\
    q_free(*CONTEXT.code);\
    free(CONTEXT.code);\
    st_free(st);\
    YYABORT;\
  }\
} while(0)

extern int yylex();
extern int yylineno;

void yyerror(const char *);
void type2str(char *buf, int);

/* The global program passed as a global variable */
extern Program *program;

/* The global symbol table */
SymTable *gst;

/* A symbol table that we fill up along the way */
SymTable *st;

/* Indication on whether a fatal error has been detected */
int yyfatal = 0;
%}
%code requires {
  #include "symtable.h"
  #include "intercode.h"
  /* I must declare this here, otherwise it thinks the type is incomplete... */
  struct expr {
    unsigned int tmp;
    int type;
    Quad **code;
    // Only use if type == SCA_ARRAY
    struct {
      int type;
      unsigned int dim;
      struct dim *dims;
    } arr;
  };
}
/* TODO Organize this */
%union {
  struct {
    char* name;
  } id;
  struct expr var;
  struct {
    unsigned long size;
    struct expr *list;
  } varlist;
  int val;
  struct {
    /* We put this in a struct so
    * we can use $$.code everywhere and be consistent
    */
    Quad **code;
  } code;
  struct {
     unsigned int dim;
     int *start;
     int *end;
  } range;
  struct parameter par;
  struct func func;
}

/* Constants */
%token <val> SCA_INT_CONST
%token <val> SCA_BOOL_CONST
%token <id> SCA_STRING_CONST

/* Keywords */
%token SCA_PROGRAM
%token SCA_VAR
%token SCA_WRITE
%token SCA_READ
%token SCA_BEGIN
%token SCA_END
%token SCA_IF
%token SCA_THEN
%token SCA_FOR
%token SCA_TO
%token SCA_DO
%token SCA_WHILE
%nonassoc SCA_DUMMY_IF
%nonassoc SCA_ELSE
%token SCA_ARRAY
%token SCA_OF
%token SCA_DOT
%token SCA_FUN
%token SCA_REF
%token SCA_RETURN

/* Ponctuation */
%token SCA_COLON
%token SCA_SEMI
%token SCA_COMMA
%token SCA_LPAR
%token SCA_RPAR
%token SCA_RBRAC
%token SCA_LBRAC

/* Types */
%token <val> SCA_INT
%token <val> SCA_BOOL
%token <val> SCA_UNIT

/* Operators */
%token SCA_AFFECT
%left SCA_EQ SCA_NE
%left SCA_GT SCA_GE SCA_LT SCA_LE
%left SCA_PLUS SCA_SUB SCA_POW SCA_OR SCA_XOR
%left SCA_MULT SCA_DIV SCA_AND
%left SCA_UMINUS

/* Identificators */
%token <id> SCA_IDENT

/* Types for each non terminal */
%type <var> expr lvalue affect arraytype funcall
%type <ids> identlist
%type <val> atomictype rangeindex rettype
%type <code> instr sequence 
%type <range> rangelist
%type <varlist> exprlist
%type <par> par
%type <func> parlist fundecl


/* Starting symbol */
%start program
%%
program: start_mark SCA_PROGRAM SCA_IDENT vardecllist fundecllist instr
       {
         free($3.name);
         YYCHECKFATAL($6);
         prg_set_code(program, *$6.code);
         free($6.code);
         prg_set_gst(program, st);
       }
       ;

start_mark: /* e */
          {
            st = st_new();
            gst = st;
          }

fundecllist: fundecl SCA_SEMI fundecllist
            | /* e */
            ;

fundecl: SCA_FUN SCA_IDENT SCA_LPAR {st_add(gst, $2.name);
                                     st_set_tmp(gst, $2.name, new_tmp());
                                     st_complete_type(gst, SCA_FUN);
                                    }
         fun_mark parlist 
         SCA_RPAR SCA_COLON rettype vardecllist { st_complete_func_type(gst, $9, $6.arity, $6.params, NULL, st); }
         instr
       {
         if ($9 == SCA_UNIT) {
           // Add automatic return if function is of unit type
           GENCODE($12, OP_RETURN, 0, 0, 0);
         }
         st_complete_code(gst, $2.name, *$12.code);
         free($2.name);
         free($6.params);
         free($12.code);
         st = gst;
       }
       ;

rettype: atomictype
       {
         $$ = $1;
       }
       | SCA_UNIT
       {
         $$ = $1;
       }
       ;

fun_mark: /* e */
        {
          gst = st;
          /* From now on, st refers to the local symbol table */
          st = st_new();
        }
        ;

parlist: par SCA_COMMA parlist
       {
         $$.arity = 1 + $3.arity;
         $$.params = malloc(sizeof(struct parameter) * $$.arity);
         memcpy($$.params, $3.params, sizeof(struct parameter) * $3.arity);
         $$.params[$$.arity - 1] = $1;
         free($3.params);
       }
       | par
       {
         $$.arity = 1;
         $$.params = malloc(sizeof(struct parameter));
         $$.params[0] = $1;
       }
       | /* empty */
       {
         $$.arity = 0;
       }
       ;

par: SCA_IDENT SCA_COLON atomictype
   {
     st_add(st, $1.name);
     st_set_tmp(st, $1.name, new_tmp());
     st_complete_type(st, $3);
     st_set_by_ref(st, $1.name, 0);
     $$ = (struct parameter) {.name = $1.name, .by_ref = 0};
     free($1.name);
   }
   | SCA_IDENT SCA_COLON arraytype
   {
     st_add(st, $1.name);
     st_set_tmp(st, $1.name, new_tmp());
     st_complete_type(st, $3.type);
     st_set_by_ref(st, $1.name, 0);
     st_complete_array_type(st, $3.arr.type, $3.arr.dim, $3.arr.dims);
     free($3.arr.dims);
     $$ = (struct parameter) {.name = $1.name, .by_ref = 0};
     free($1.name);
   }
   | SCA_REF SCA_IDENT SCA_COLON atomictype
   {
     st_add(st, $2.name);
     st_set_tmp(st, $2.name, new_tmp());
     st_complete_type(st,  $4);
     st_set_by_ref(st, $2.name, 1);
     $$ = (struct parameter) {.name = $2.name, .by_ref = 1};
     free($2.name);
   }
   | SCA_REF SCA_IDENT SCA_COLON arraytype
   {
     st_add(st, $2.name);
     st_set_tmp(st, $2.name, new_tmp());
     st_complete_type(st, $4.type);
     st_set_by_ref(st, $2.name, 1);
     st_complete_array_type(st, $4.arr.type, $4.arr.dim, $4.arr.dims);
     free($4.arr.dims);
     $$ = (struct parameter) {.name = $2.name, .by_ref = 1};
     free($2.name);
   }
   ;

vardecllist: varsdecl SCA_SEMI vardecllist
          | varsdecl
          | /* e */
          ;

varsdecl: SCA_VAR identlist SCA_COLON atomictype
        {
          st_complete_type(st, $4);
        }
        | SCA_VAR identlist SCA_COLON arraytype
        {
          st_complete_type(st, SCA_ARRAY);
          st_complete_array_type(st, $4.arr.type, $4.arr.dim, $4.arr.dims);
          free($4.arr.dims);
        }
        ;

identlist: SCA_IDENT
         {
           st_add(st, $1.name);
           st_set_tmp(st, $1.name, new_tmp());
           free($1.name);
         }
         | identlist SCA_COMMA SCA_IDENT
         {
            st_add(st, $3.name);
            st_set_tmp(st, $3.name, new_tmp());
            free($3.name);
         }
         ;

atomictype: SCA_INT
          {
            $$ = $1;
          }
          | SCA_BOOL
          {
            $$ = $1;
          }
          ;

arraytype: SCA_ARRAY SCA_LBRAC rangelist SCA_RBRAC SCA_OF atomictype
         {
           $$.type = SCA_ARRAY;
           $$.arr.type = $6;
           $$.arr.dims = malloc($3.dim * sizeof(struct dim));
           $$.arr.dim = $3.dim;
           for (unsigned int i = 0; i < $3.dim; i++) {
             $$.arr.dims[i].start_index = $3.start[i];
             $$.arr.dims[i].size = ($3.end[i] - $3.start[i]) + 1;
           }
           free($3.start);
           free($3.end);
         }
         ;

rangelist: rangeindex SCA_DOT SCA_DOT rangeindex
         {
           if ($1 > $4) {
             ERROR("Array range invalid (first index > last index).%s\n", "");
             yyfatal = 1;
           }
           $$.dim = 1;
           $$.start = malloc(sizeof(int));
           $$.end = malloc(sizeof(int));
           $$.start[0] = $1;
           $$.end[0] = $4;
         }
         | rangeindex SCA_DOT SCA_DOT rangeindex SCA_COMMA rangelist
         {
           if ($1 > $4) {
             ERROR("Array range invalid (first index > last index).%s\n", "");
             yyfatal = 1;
           }
           $$.dim = $6.dim + 1;
           $$.start = malloc($$.dim * sizeof(int));
           $$.end = malloc($$.dim * sizeof(int));
           memcpy($$.start, $6.start, $6.dim * sizeof(int));
           memcpy($$.end, $6.end, $6.dim * sizeof(int));
           free($6.start);
           free($6.end);
           $$.start[$$.dim - 1] = $1;
           $$.end[$$.dim - 1] = $4;
         }
         ;

rangeindex: SCA_INT_CONST
          {
            $$ = $1;
          }
          | SCA_SUB SCA_INT_CONST
          {
            $$ = -$2;
          }
          ;

affect: lvalue SCA_AFFECT expr
     {
       AFFECT_ERROR($1, $3);
       COPY_CODE($$, $3);
       // If this is an array, we need to complete the instruction
       if ($1.type == SCA_ARRAY) {
          // A bit ugly since we have to know it's the last code of the list.
          SET_LAST($1, x, $3.tmp);
          CONCAT_CODE($$, $1);
        }
       else
         GENCODE($$, OP_AFFECT, $3.tmp, 0, $1.tmp);
       $$.tmp = $1.tmp;
     }
     ;

instr: affect
     {
       COPY_CODE($$,$1);
     }
     | funcall
     {
       COPY_CODE($$, $1);
     }
     | SCA_RETURN
     {
       INIT_CODE_WITH($$, OP_RETURN, 0, 0, 0);
     }
     | SCA_RETURN expr
     {
       COPY_CODE($$, $2);
       GENCODE($$, OP_RETURN, $2.tmp, 0, 0);
     }
     | SCA_IF expr SCA_THEN instr %prec SCA_DUMMY_IF
     {
       unsigned int label = new_label();
       COPY_CODE($$, $2);
       GENCODE($$, OP_IF_NOT_GOTO, $2.tmp, label, 0);
       CONCAT_CODE($$, $4);
       GENCODE($$, OP_LABEL, label, 0, 0);
     }
     | SCA_FOR affect SCA_TO expr SCA_DO instr
     {
       unsigned int i = new_tmp();
       unsigned int b = new_tmp();
       unsigned int label1 = new_label();
       unsigned int label2 = new_label();
       COPY_CODE($$,$2);
       GENCODE($$, OP_LABEL, label1, 0, 0);
       CONCAT_CODE($$,$4);
       GENCODE($$, OP_LT, $2.tmp, $4.tmp, b);
       GENCODE($$, OP_IF_NOT_GOTO, b, label2, 0);
       CONCAT_CODE($$, $6);
       GENCODE($$, OP_AFFECTI, 1, 0, i);
       GENCODE($$,OP_PLUS, i, $2.tmp, $2.tmp);
       GENCODE($$,OP_GOTO, label1, 0, 0);
       GENCODE($$,OP_LABEL, label2, 0, 0);
     }
     | SCA_IF expr SCA_THEN instr SCA_ELSE instr
     {
       unsigned int label1 = new_label();
       unsigned int label2 = new_label();
       COPY_CODE($$, $2);
       GENCODE($$, OP_IF_NOT_GOTO, $2.tmp, label1, 0);
       CONCAT_CODE($$, $4);
       GENCODE($$, OP_GOTO, label2, 0, 0);
       GENCODE($$, OP_LABEL, label1, 0, 0);
       CONCAT_CODE($$, $6);
       GENCODE($$, OP_LABEL, label2, 0, 0);
     }
     | SCA_BEGIN sequence SCA_END
     {
       COPY_CODE($$, $2);
     }
     | SCA_BEGIN SCA_END
     {
       INIT_CODE($$);
     }
     | SCA_WRITE expr
     {
       COPY_CODE($$, $2);
       switch($2.type) {
         case SCA_INT:
           GENCODE($$, OP_WRITE_INT, $2.tmp, 0, 0);
           break;
         case SCA_BOOL:
           GENCODE($$, OP_WRITE_BOOL, $2.tmp, 0, 0);
         break;
       }
     }
     | SCA_WRITE SCA_STRING_CONST
     {
       unsigned int tmp = new_tmp();
       st_add(gst, $2.name);
       st_set_tmp(gst, $2.name, tmp);
       st_complete_type(gst, SCA_STRING_CONST);
       INIT_CODE_WITH($$, OP_WRITE_STRING, tmp, 0, 0);
       free($2.name);
     }
     | SCA_READ lvalue
     {
       unsigned int tmp = new_tmp();
       INIT_CODE_WITH($$, OP_READ, tmp, 0, 0);
       if ($2.type == SCA_ARRAY) {
         SET_LAST($2, x, tmp);
         CONCAT_CODE($$, $2);
       }
       else
         GENCODE($$, OP_AFFECT, tmp, 0, $2.tmp);
     }
     | SCA_WHILE expr SCA_DO instr
     {
       unsigned int label1 = new_label();
       unsigned int label2 = new_label();
       INIT_CODE_WITH($$, OP_LABEL, label1, 0, 0);
       CONCAT_CODE($$, $2);
       GENCODE($$, OP_IF_NOT_GOTO, $2.tmp, label2, 0);
       CONCAT_CODE($$, $4);
       GENCODE($$, OP_GOTO, label1, 0, 0);
       GENCODE($$, OP_LABEL, label2, 0, 0);
     }
     ;

sequence: instr SCA_SEMI sequence
        {
          COPY_CODE($$, $1);
          CONCAT_CODE($$, $3);
        }
        | instr SCA_SEMI
        {
          COPY_CODE($$, $1);
        }
        | instr
        {
          COPY_CODE($$, $1);
        }
        ;

lvalue: SCA_IDENT
      {
        Entry id;
        FETCH_ID(st, gst, $1.name);
        free($1.name);
        $$.tmp = id.tmp;
        $$.type = id.type;
      }
      | SCA_IDENT SCA_LBRAC exprlist SCA_RBRAC
      {
        Entry id;
        FETCH_ID(st, gst, $1.name);
        if (id.name != NULL && id.type != -1)
          ARRAY_ERROR(id);
        free($1.name);
        if (id.type == SCA_ARRAY) {
          $$.tmp = id.tmp;
          $$.type = id.type;
          $$.arr.type = id.arr.type;
          // The first argument of the operation, which is the value to put
          // in the array is unknown at this time. It will be completed
          // when treating the affect aciton.
          INIT_CODE($$);
          unsigned int acc = new_tmp();
          COMPUTE_INDEX($$, $3, id, acc);
          // At this point, the *real* index in the array is in `acc`
          GENCODE($$, OP_WRITE_ARR, 0, acc, $$.tmp);
        }
        else {
          for (unsigned int i = 0; i < $3.size; i++) {
             q_free(*$3.list[i].code);
             free($3.list[i].code);
          }
          INIT_CODE($$);
        }
        free($3.list);
      }
      ;

exprlist: expr
        {
          $$.size = 1;
          $$.list = malloc(sizeof(struct expr));
          $$.list[0] = $1;
        }
        | expr SCA_COMMA exprlist
        {
          $$.size = 1 + $3.size;
          $$.list = malloc(sizeof(struct expr) * $$.size);
          memcpy($$.list, $3.list, $3.size * sizeof(struct expr));
          free($3.list);
          $$.list[$$.size - 1] = $1;
        }
        ;

funcall: SCA_IDENT SCA_LPAR exprlist SCA_RPAR
       {
         Entry id;
         FETCH_ID(gst, gst, $1.name);
         if (id.type != -1 && id.name != NULL)
           CALL_ERROR(id);
         INIT_CODE_WITH($$, OP_PRE_CALL, 0, 0, 0);
         for (unsigned long i = $3.size; i > 0; i--) {
           CONCAT_CODE($$, $3.list[i - 1]);
           if (id.type == SCA_FUN)
             GENCODE($$, OP_PARAM, $3.list[i - 1].tmp, i - 1, id.func.params[i - 1].by_ref);
         }
         GENCODE($$, OP_CALL, id.tmp, 0, 0);
         free($3.list);
         free($1.name);
         $$.type = id.func.ret_type;
       }
       | SCA_IDENT SCA_LPAR SCA_RPAR
       {
         Entry id;
         FETCH_ID(gst, gst, $1.name);
         if (id.type != -1 && id.name != NULL)
           CALL_ERROR(id);
         INIT_CODE_WITH($$, OP_CALL, id.tmp, 0, 0);
         free($1.name);
         $$.type = id.func.ret_type;
       }
       ;

 /* TODO? factor those operations into other symbols and use types
 * to differenciate ?
 * seems like a lot of work for not much, i don't know _/(^m^)\_
 */
expr: funcall
    {
      $$.tmp = new_tmp();
      $$.type = $1.type;
      COPY_CODE($$, $1);
      SET_LAST($$, res, $$.tmp);
    }
    | SCA_INT_CONST
    {
      $$.tmp = new_tmp();
      $$.type = SCA_INT;
      INIT_CODE_WITH($$, OP_AFFECTI, $1, 0, $$.tmp);
    }
    | SCA_BOOL_CONST

    {
      $$.tmp = new_tmp();
      $$.type = SCA_BOOL;
      INIT_CODE_WITH($$, OP_AFFECTI, $1, 0, $$.tmp);
    }
    | SCA_LPAR expr SCA_RPAR
    {
      $$ = $2;
    }
    | SCA_IDENT
    {
      Entry id;
      FETCH_ID(st, gst, $1.name);
      free($1.name);
      $$.tmp = id.tmp;
      $$.type = id.type;
      INIT_CODE($$);
    }
    | SCA_IDENT SCA_LBRAC exprlist SCA_RBRAC
    {
      Entry id;
      FETCH_ID(st, gst, $1.name);
      if (id.name != NULL && id.type != -1)
        ARRAY_ERROR(id);
      free($1.name);
      if (id.type == SCA_ARRAY) {
        $$.type = id.arr.type;
        $$.tmp = new_tmp();
        INIT_CODE($$);
        unsigned int acc = new_tmp();
        COMPUTE_INDEX($$, $3, id, acc);
        GENCODE($$, OP_READ_ARR, id.tmp, acc, $$.tmp);
      }
      else {
        for (unsigned int i = 0; i < $3.size; i++) {
           q_free(*$3.list[i].code);
           free($3.list[i].code);
        }
        INIT_CODE($$);
      }
      free($3.list);
    }
    | SCA_SUB expr %prec SCA_UMINUS
    {
      if ($2.type != SCA_INT) {
        ERROR("the unary '-' operator can only be used on expressions of type "
              "'int'%s", "");
        yyfatal = 1;
      }
      $$.tmp = new_tmp();
      $$.type = $2.type;
      COPY_CODE($$, $2);
      GENCODE($$, OP_NEG, $2.tmp, 0, $$.tmp);
    }
    | expr SCA_PLUS expr
    {
      OP_ERROR($1, "add", $3);
      BINOP(OP_PLUS, $$, $1, $3);
    }
    | expr SCA_SUB expr
    {
      OP_ERROR($1, "subtract", $3);
      BINOP(OP_SUB, $$, $1, $3);
    }
    | expr SCA_MULT expr
    {
      OP_ERROR($1, "multiply", $3);
      BINOP(OP_MULT, $$, $1, $3);
    }
    | expr SCA_DIV expr
    {
      OP_ERROR($1, "divide", $3);
      BINOP(OP_DIV, $$, $1, $3);
    }
    | expr SCA_POW expr
    {
      OP_ERROR($1, "power", $3);
      BINOP(OP_POW, $$, $1, $3);
    }
    | expr SCA_OR expr
    {
      OP_ERROR($1, "or", $3);
      $$.type = $1.type ;
      $$.tmp = new_tmp();
      unsigned int lab1 = new_label();
      unsigned int lab2 = new_label();
      unsigned int labend = new_label();
      COPY_CODE($$, $1);
      GENCODE($$,OP_IF_NOT_GOTO, $1.tmp, lab1, 0);
      GENCODE($$,OP_AFFECTI, 1, 0, $$.tmp); 
      GENCODE($$,OP_GOTO, labend, 0, 0);
      GENCODE($$,OP_LABEL, lab1, 0, 0);
      GENCODE($$,OP_IF_NOT_GOTO, $3.tmp, lab2, 0);
      GENCODE($$,OP_AFFECTI, 1, 0, $$.tmp);
      GENCODE($$,OP_GOTO,labend, 0 ,0);
      GENCODE($$,OP_LABEL, lab2, 0, 0);
      GENCODE($$,OP_AFFECTI, 0, 0, $$.tmp);
      GENCODE($$,OP_LABEL, labend, 0, 0);
    }
    | expr SCA_AND expr
    {
      OP_ERROR($1, "and", $3);
      $$.type = $1.type ;
      $$.tmp = new_tmp();
      unsigned int lab1 = new_label();
      unsigned int lab2 = new_label();
      unsigned int labend = new_label();
      COPY_CODE($$, $1);
      GENCODE($$,OP_IF_GOTO, $1.tmp, lab1, 0);
      GENCODE($$,OP_AFFECTI, 0, 0, $$.tmp); 
      GENCODE($$,OP_GOTO, labend, 0, 0);
      GENCODE($$,OP_LABEL, lab1, 0, 0);
      GENCODE($$,OP_IF_GOTO, $3.tmp, lab2, 0);
      GENCODE($$,OP_AFFECTI, 0, 0, $$.tmp);
      GENCODE($$,OP_GOTO,labend,0,0);
      GENCODE($$,OP_LABEL, lab2, 0, 0);
      GENCODE($$,OP_AFFECTI, 1, 1, $$.tmp);
      GENCODE($$,OP_LABEL, labend, 0, 0);
    }
    | expr SCA_XOR expr
    {
      OP_ERROR($1, "xor", $3);
      BINOP(OP_XOR, $$, $1, $3);
    }
    | expr SCA_EQ expr
    {
      OP_ERROR($1, "compare", $3);
      COMP(OP_EQ, $$, $1, $3);
    }
    | expr SCA_NE expr
    {
      OP_ERROR($1, "compare", $3);
      COMP(OP_NE, $$, $1, $3);
    }
    | expr SCA_LT expr
    {
      OP_ERROR($1, "compare", $3);
      COMP_ERROR($1);
      COMP(OP_LT, $$, $1, $3);
    }
    | expr SCA_GT expr
    {
      OP_ERROR($1, "compare", $3);
      COMP_ERROR($1);
      COMP(OP_GT, $$, $1, $3);
    }
    | expr SCA_LE expr
    {
      OP_ERROR($1, "compare", $3);
      COMP_ERROR($1);
      COMP(OP_LE, $$, $1, $3);
    }
    | expr SCA_GE expr
    {
      OP_ERROR($1, "compare", $3);
      COMP_ERROR($1);
      COMP(OP_GE, $$, $1, $3);
    }
    ;

%%
void yyerror(const char *msg) {
  fprintf(stderr, "line %d: %s\n", yylineno, msg);
}
