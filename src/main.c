#include <getopt.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "cfg.h"
#include "intercode.h"
#include "opti.h"
#include "options.h"
#include "parser.h"
#include "program.h"
#include "symtable.h"
#include "tr_mips.h"
#include "utils.h"

Program *program;

extern void yylex_destroy();
extern FILE *yyin;

void print_usage(const char *cmd) {
  fprintf(stderr, "Usage: %s [OPTION]... FILE\n", cmd);
}

void print_ver() {
  fprintf(stdout, "SCALPA Compiler %s\n---\nAuthors : %s\n", VERSION, AUTHORS);
}

void print_help(const char *cmd) {
  print_usage(cmd);
  fprintf(stdout, "Compile a SCALPA source code to MIPS assembly.\n\n");
  for (int i = 0; option_helps[i].option != NULL; i++) {
    fprintf(stdout, "  %-20s %s\n\n", option_helps[i].option,
            option_helps[i].help);
  }
}

int main(int argc, char **argv) {
  /* Option managment, this can be done in a separate function */
  int c, opt_index, i;
  int intcode = 0, tos = 0, opti_lvl = -1;
  CFG *cfg;
  const char *out_file = "out.s";
  const char *cfg_path = "";
  while ((c = getopt_long(argc, argv, "c:O:o:tihv", long_options,
                          &opt_index)) != -1) {
    switch (c) {
    case 'O':
      opti_lvl = atoi(optarg);
      break;
    case 'c':
      cfg_path = optarg;
      break;
    case 'v':
      print_ver();
      exit(EXIT_SUCCESS);
      break;
    case 't':
      tos = 1;
      break;
    case 'o':
      out_file = optarg;
      break;
    case 'i':
      intcode = 1;
      break;
    case 'h':
      print_help(argv[0]);
      exit(EXIT_SUCCESS);
      break;
    default:
      exit(EXIT_FAILURE);
      break;
    }
  }

  if (optind == argc - 1) {
    yyin = fopen(argv[optind], "r");
    TEST_ERROR(yyin, ==, NULL);
  } else {
    if (optind < argc - 1)
      fprintf(stderr, "%s : too many arguments.\n", argv[0]);
    else if (optind >= argc)
      fprintf(stderr, "%s : one argument required\n", argv[0]);
    print_usage(argv[0]);
    exit(EXIT_FAILURE);
  }

  /* Actual code */
  /* ----------- */
  program = prg_new();
  if (yyparse() != 0) {
    fclose(yyin);
    yylex_destroy();
    prg_free(program);
    exit(EXIT_FAILURE);
  }
  TEST_ERROR(fclose(yyin), ==, EOF);

  FILE *out = fopen(out_file, "w");
  TEST_ERROR(out, ==, NULL);
  tr_prg(program, out);

  cfg = create_cfg(program);
  if (strcmp(cfg_path, "")) {
    export_cfg(cfg, cfg_path);
  }
  if (opti_lvl > 0) {
    for (i = 0; i < opti_lvl; i++) {
      program = optimization(program, cfg);
    }
  }
  free_cfg(cfg);

  if (intcode)
    q_listprint(program->code);
  if (tos)
    st_print(program->gst, intcode);
  prg_free(program);
  TEST_ERROR(fclose(out), ==, EOF);
  yylex_destroy();
}
