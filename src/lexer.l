%option nounput
%option noyywrap
%option noinput
%option yylineno
%{
//TODO ^ %option noinput is used because of a warning. Remove later.
#include "parser.h"
#include "utils.h"
#include <string.h>
#include "parser.h"

extern YYSTYPE yylval;
%}
%%
program { return SCA_PROGRAM; }
var { return SCA_VAR; }
write { return SCA_WRITE; }
read { return SCA_READ; }
begin { return SCA_BEGIN; }
end { return SCA_END; }
int { yylval.val = SCA_INT; return SCA_INT; }
bool { yylval.val = SCA_BOOL; return SCA_BOOL; }
true { yylval.val = 1; return SCA_BOOL_CONST; }
false { yylval.val = 0; return SCA_BOOL_CONST; }
if { return SCA_IF; }
then { return SCA_THEN; }
for { return SCA_FOR; }
to { return SCA_TO; }
do { return SCA_DO; }
while { return SCA_WHILE; }
else { return SCA_ELSE; }
array { return SCA_ARRAY; }
of { return SCA_OF; }
function { return SCA_FUN; }
return { return SCA_RETURN; }
unit { yylval.val = SCA_UNIT; return SCA_UNIT; }
ref { return SCA_REF; }
\. { return SCA_DOT; }

: { return SCA_COLON; }
; { return SCA_SEMI; }
, { return SCA_COMMA; }
\( { return SCA_LPAR; }
\) { return SCA_RPAR; }
\[ { return SCA_LBRAC; }
\] { return SCA_RBRAC; }

:= { return SCA_AFFECT; }
\+ { return SCA_PLUS; }
\* { return SCA_MULT; }
\/ { return SCA_DIV; }
\- { return SCA_SUB; }
\^ { return SCA_POW; }
or { return SCA_OR; }
xor { return SCA_XOR; }
and { return SCA_AND; }
= { return SCA_EQ; }
\<\> { return SCA_NE; }
\< { return SCA_LT; }
\> { return SCA_GT; }
\<= { return SCA_LE; }
\>= { return SCA_GE; }

\"([^"]|\\\")*\" {
              yylval.id.name = malloc(strlen(yytext) + 1);
              TEST_ERROR_W_MSG(snprintf(yylval.id.name, 
                    strlen(yytext) + 1, "%s", yytext), >, (int) strlen(yytext),
                    "Fatal error: snprintf failed\n");
              return SCA_STRING_CONST;
            }

0|([1-9]+[0-9]*) { yylval.val = atoi(yytext); return SCA_INT_CONST; }

([A-Za-z_]+)([A-Za-z0-9_]*) {
                              yylval.id.name = malloc(strlen(yytext) + 1);
                              TEST_ERROR_W_MSG(snprintf(yylval.id.name,
                              strlen(yytext) + 1,"%s",yytext), >, (int)strlen(yytext), 
                              "Fatal error : snprint failed, output witten Wrong in lexer\n");
                              return SCA_IDENT;
                            }
[[:space:]]+ { }
