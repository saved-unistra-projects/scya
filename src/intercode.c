#include <stdio.h>
#include <stdlib.h>

#include "intercode.h"

Quad *q_new(enum quad_op op, unsigned int x, unsigned int y, unsigned int res) {
  Quad *q;
  if ((q = malloc(sizeof(Quad))) == NULL)
    return NULL;
  q->op = op;
  q->x = x;
  q->y = y;
  q->res = res;
  q->next = NULL;
  return q;
}

void q_concat(Quad *l1, Quad *l2) {
  Quad *cur = l1;
  while (cur->next != NULL) {
    cur = cur->next;
  }
  cur->next = l2;
}

Quad *get_last(Quad *l) {
  while (l->next != NULL) {
    l = l->next;
  }
  return l;
}

void q_free(Quad *l) {
  Quad *next = l;
  while (next != NULL) {
    next = l->next;
    free(l);
    l = next;
  }
}

void q_print(Quad *l) {
  // TODO Cases to print each call correctly
  if (l->op == OP_NOOP || l->op == OP_PRE_CALL)
    return;
  // Special print case for arrays
  switch (l->op) {
  case OP_READ_ARR:
    fprintf(stdout, "T%u := T%u[T%u]\n", l->res, l->x, l->y);
    return;
  case OP_WRITE_ARR:
    fprintf(stdout, "T%u[T%u] := T%u\n", l->res, l->y, l->x);
    return;
  default:
    break;
  }
  if (!l->res) {
    switch (l->op) {
    case OP_WRITE_BOOL:
    case OP_WRITE_INT:
    case OP_WRITE_STRING:
      fprintf(stdout, "write T%u\n", l->x);
      return;
    case OP_READ:
      fprintf(stdout, "read T%u\n", l->x);
      return;
    case OP_IF_NOT_GOTO:
      fprintf(stdout, "if not T%u goto L%u\n", l->x, l->y);
      return;
    case OP_IF_GOTO:
      fprintf(stdout, "if T%u goto L%u\n", l->x, l->y);
      return;
    case OP_GOTO:
      fprintf(stdout, "goto L%u\n", l->x);
      return;
    case OP_LABEL:
      fprintf(stdout, "L%u:\n", l->x);
      return;
    case OP_CALL:
      fprintf(stdout, "call T%u\n", l->x);
      return;
    case OP_RETURN:
      if (l->x != 0)
        fprintf(stdout, "return T%u\n", l->x);
      else
        fprintf(stdout, "return\n");
      return;
    default:
      break;
    }
  }
  if (l->res)
    fprintf(stdout, "T%u := ", l->res);
  if (!l->y) {
    switch (l->op) {
    case OP_AFFECT:
      printf("T%u\n", l->x);
      return;
    case OP_AFFECTI:
      printf("%d\n", l->x);
      return;
    case OP_NEG:
      printf("-T%u\n", l->x);
      return;
    case OP_CALL:
      printf("call T%u\n", l->x);
      return;
    default:
      break;
    }
  }
  const char *op;
  switch (l->op) {
  case OP_POW:
    op = "^";
    break;
  case OP_PARAM:
    fprintf(stdout, "param T%u %s (param number %u)\n", l->x,
            l->res ? "by ref" : "", l->y);
    return;
  case OP_MULT:
    op = "*";
    break;
  case OP_PLUS:
    op = "+";
    break;
  case OP_DIV:
    op = "/";
    break;
  case OP_SUB:
    op = "-";
    break;
  case OP_AND:
    op = "and";
    break;
  case OP_OR:
    op = "or";
    break;
  case OP_XOR:
    op = "xor";
    break;
  case OP_EQ:
    op = "=";
    break;
  case OP_NE:
    op = "=/=";
    break;
  case OP_LT:
    op = "<";
    break;
  case OP_GT:
    op = ">";
    break;
  case OP_LE:
    op = "<=";
    break;
  case OP_GE:
    op = ">=";
    break;
  default:
    op = "?";
    break;
  }
  fprintf(stdout, "T%u %s T%u\n", l->x, op, l->y);
}

void q_listprint(Quad *l) {
  while (l != NULL) {
    q_print(l);
    l = l->next;
  }
}

unsigned int new_tmp() {
  static unsigned int cur_tmp = 0;
  cur_tmp += 1;
  return cur_tmp;
}

unsigned int new_label() {
  static unsigned int cur_label = 0;
  cur_label += 1;
  return cur_label;
}
