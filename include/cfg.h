/*! \file cfg.h
 *  \brief Generation and export of Control-Flow Graphes (CFG)
 */

#ifndef __CFG_H__
#define __CFG_H__

#include "program.h"
#include "utils.h"
#include <stdio.h>

/*! \brief A structure representing a block of the CFG
 *
 * Each block has a pointer to the next one, even if its not its branching,
 * so we can easily iterate through them. Thus, they form a NULL terminated
 * stack.
 *
 * They carry a ref to the first line of code associated in the program for
 * easy modifications, and \c length represents the number of quads in the
 * block.
 *
 * \c label is the label number if the block starts with a label or -1.
 *
 * \c goto_branch_label and \c cond_branch_label are the label numbers of the
 * possible branchings of the block. They allow to access easily to the
 * associated blocks thanks the array label of the CFG structure.
 */
typedef struct Block {
  unsigned int id;
  int label;
  Quad *code;
  unsigned int length;
  struct Block *next;
  int goto_branch_label;
  int cond_branch_label;
} Block;

/*! \brief A structure representing a CFG of a function.
 *
 * Forms a stack of CFGs of function, with at least the main one. It ends with
 * NULL.
 *
 * \c label is an array of blocks representing the first one of the CFG and
 * and every of its blocks starting with a label for an easy access to the
 * branching targets.
 */
typedef struct CFG {
  Block **label;
  const char *name;
  struct CFG *next;
} CFG;

/*! \brief Create a CFG for a program.
 *
 *  \param program is a structure representing the intermediate code.
 *
 *  \returns a pointer to the created CFG structure.
 */
CFG *create_cfg(Program *program);

/*! \brief Convert the code of a function into a labels array (see label
 * the structure CFG).
 *
 * \param code is a pointer to the code of a function.
 *
 * \returns an array of pointers to blocks (0 is the beginning of the control
 * flow tree and the others are blocks of it starting with a label).
 */
Block **code_to_cft(Quad *code);

/*! \brief Free a CFG
 *
 * \param cfg is a pointer to the CFG to free
 *
 * \sidef Because of the stack-structure of the CFG, each tree is a pointer to
 * CFG and thus, they're all freed in the process. Each block of each CFG is
 * also freed.
 */
void free_cfg(CFG *cfg);

/*! \brief Export the CFG into a DOT graph.
 *
 * \param cfg is a pointer to CFG we want to export.
 *
 * \param path is the path of the file we want to use to save the graph. It is
 * expected to end as '.dot' even though its not mandatory.
 *
 * \sidef The operation overwrite any existing file named as \param path.
 */
void export_cfg(CFG *cfg, const char *path);

#endif // __CFG_H__