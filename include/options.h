/*! \file options.h
 * \brief Option related variables and definitions.
 */

#ifndef __OPTIONS_H__
#define __OPTIONS_H__

#include <getopt.h>
#include <stdlib.h>

//! List of the author names.
#define AUTHORS                                                                \
  "COLIN Raphaël, DUHAMEL Thomas, REILAND Valentine, SCHIEBER Morgane"

//! Current software version, use by `--version`.
#define VERSION "1.0.2"

//! This struct represents an help entry when using the `--help` option.
struct help {
  const char *option; //!< A string representing the option name.
  const char
      *help; //!< A string representing the help associated with the option.
};

/*! \brief Help structure for each existing option.
 *
 * When adding options to the program, add its help structure here.
 *
 * \note See struct ::help
 */
struct help option_helps[] = {
    {"--version", "Print version info and authors"},
    {"-t, --tos", "Print the table of symbols"},
    {"-i, --intcode", "Print the generated intermediate code"},
    {"-h, --help", "Print this help and exit"},
    {"-Ok", "Run k rounds of optimization"},
    {"-c, --cfg=PATH",
     "Draw the control-flow graph in the DOT file pointed by PATH"},
    {"-o, --output=PATH",
     "Output the resulting assembly code in the file pointed by PATH"},
    {NULL, NULL},
};

/*! \brief This is used by `getopt_long` to parse the options of the program.
 *
 * \note See the man page for `getopt_long` for more info.
 */
struct option long_options[] = {
    {"version", no_argument, 0, 'v'},
    {"tos", no_argument, 0, 't'},
    {"intcode", no_argument, 0, 'i'},
    {"help", no_argument, 0, 'h'},
    {"output", required_argument, 0, 'o'},
    {"cfg", required_argument, 0, 'c'},
    {NULL, 0, NULL, 0},
};

#endif // __OPTIONS_H__
