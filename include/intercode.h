/*! \file intercode.h
 *  \brief intermediate code represented by quadruplets.
 *
 *  This file contains functions and structures that represent and manipulate
 *  quadruplets, which are a representation of 3 address code (3AC).
 */

#ifndef __INTERCODE_H__
#define __INTERCODE_H__

/*! \brief Represents the different kind of operations possible in 3AC.
 *
 * This enum lists all possible operations. They occupy the `op` member
 * of the `quad` struct.
 * Some members will have examples in more "readable" 3AC in parenthesis next to
 * them.
 */
enum quad_op {
  OP_NOOP,      //!< No operation.
  OP_AFFECT,    //!< Affectation (T1 := T2).
  OP_AFFECTI,   //!< Immediate value affectation (T1 := 2).
  OP_READ_ARR,  //!< Read a value from an array (T1 := T2[T3]).
  OP_WRITE_ARR, //!< Write a value in an array (T1[T2] := T3).
  OP_NEG,       //!< Integer unary minus (T1 := - T2).
  OP_POW,       //!< Interger power (T1 := T2 ^ T3).
  OP_MULT,      //!< Integer multiplication (T1 := T2 * T3).
  OP_PLUS,      //!< Integer addition (T1 := T2 + T3).
  OP_DIV,       //!< Integer division (T1 := T2 / T3).
  OP_SUB,       //!< Integer substraction (T1 := T2 - T3).
  OP_AND,       //!< Logical and (T1 := T2 and T3).
  OP_OR,        //!< Logical or (T1 := T2 or T3).
  OP_XOR,       //!< Logical exclusive or (T1 := T2 xor T3).
  OP_EQ,        //!< Equal comparison on booleans or integers
                //!< (T1 := T2 = T3, T1 holds a boolean now).
  OP_NE, //!< Not equal comparison on booleans or integers (T1 := T2 != T3).
  OP_LT, //!< Lesser than comparison on integers (T1 := T2 < T3).
  OP_GT, //!< Greater than comparison on integers (T1 := T2 > T3).
  OP_LE, //!< Lesser or equal comparison on integers (T1 := T2 <= T3).
  OP_GE, //!< Greater or equal comparison on integers (T1 := T2 >= T3).
  OP_IF_NOT_GOTO, //!< Conditionnal branching (if not T1 goto L1, here, T1 is a
                  //!< boolean and L1 is a label). Jumps if T1 is false.
  OP_IF_GOTO, //!< Conditionnal branching (if T1 goto L1). Jumps if T1 is true.
  OP_GOTO,    //!< Unconditionnal branching (goto L1, L1 is a label).
  OP_READ,    //!< Read from standard input (read T1). T1 will hold the value
              //!< read on the input stream.
  OP_WRITE_INT,    //!< Write an integer to standard output (write T1).
  OP_WRITE_BOOL,   //!< Write a boolean to standard output (write T1).
  OP_WRITE_STRING, //!< Write a string to standard output.
  OP_LABEL,        //!< Place a label (L1: *instructions*).
  OP_PARAM,        //!< Add a parameter (for a function call). res is a boolean
            //!< indicating whether the parameter should be passed by reference
            //!< of not.
  OP_PRE_CALL, //!< Empty operation, just to notify the translator we're about
               //!< to call a function
  OP_CALL,     //!< Call a subroutine.
  OP_RETURN,   //!< Return from a subroutine (with a value eventually).
};

/*! \brief A structure representing a quadruplet and a list of quadruplets.
 *
 * Operands have different meaning depending on the operation. This should be
 * detailed somewhere later.
 *
 * Each quadruplet has a pointer to the next one in the list.
 *
 * Some operands might be useless for certain operations. Those should be set
 * to `0` by convention.
 *
 * \c x, \c y and \c res are `unsigned int` values that represent the
 * number of a temporary variable in 3AC. For example, the quadruplet
 * `(OP_MULT, 2, 3, 1)` means `T1 := T2 * T3` in 3AC.
 */
typedef struct quad {
  enum quad_op op;   //!< Operation. Must be a member of ::quad_op.
  unsigned int x;    //!< First operand.
  unsigned int y;    //!< Second operand.
  unsigned int res;  //!< Result of the operation.
  struct quad *next; //!< Pointer to the next quadruplet in the list.
                     /*!<
                      *  equal to `NULL` if there is no quadruplet following
                      *  this one.
                      */
} Quad;

/*! \brief Create a new quadruplet list.
 *
 * This function will allocate memory for a new quadruplet or quadruplet list.
 *
 * \param op the operation of the quadruplet.
 * \param x first operand.
 * \param y second operand.
 * \param res result temporary variable.
 *
 * \returns a pointer to a quadruplet structure, or \c NULL if it fails.
 */
Quad *q_new(enum quad_op op, unsigned int x, unsigned int y, unsigned int res);

/*! \brief Concatenate 2 existing quadruplet lists.
 *
 * Will add \p l2 at the end of \p l1.
 *
 * \param l1 the initial list.
 * \param l2 the list to add at the end.
 *
 * \sidef Changes the `next` pointer of the last element of \p l1 to
 * the pointer of the first element of \p l2, effectively concatenating the two
 * lists. The result of the concatenation is therefore in \p l1.
 */
void q_concat(Quad *l1, Quad *l2);

/*! \brief Get the last quadruplet of a list.
 *
 * \param l a quadruplet list.
 *
 * \returns a pointer to the last element of the list.
 */
Quad *get_last(Quad *l);

/*! \brief Free a quadruplet list.
 *
 * \param l the list to free.
 *
 * \sidef Frees every element of the list as well as the pointer to the list
 * itself. Any access to any member of the list after a call to this function
 * shall result in unexpected behavior.
 */
void q_free(Quad *l);

/*! \brief Print a single quadruplet to standard output.
 *
 * \param q the quadruplet to print.
 *
 * \sidef Prints a representation of the quadruplet in 3AC format to standard
 * output.
 */
void q_print(Quad *q);

/*! \brief Print a list of quadruplets.
 *
 * \param l the list to print.
 *
 * \sidef Calls ::q_print on every quadruplet of the list, effectively printing
 * the whole list of quadruplets to standard output.
 */
void q_listprint(Quad *l);

/*! \brief Return an integer representing a new temporary variable number.
 *
 * \returns a new variable number
 *
 * \sidef Increases the internal count of used variable numbers.
 *
 * \note The returned value is guaranteed to be different from every other
 * value returned by this function before. Except if the integer overflows,
 * which should not happen with a reasonnable number of variables.
 *
 * \warning Not thread safe.
 */
unsigned int new_tmp();

/*! \brief Return an integer representing a new label number.
 *
 * Very similar to ::new_tmp.
 *
 * \returns an integer representing a label.
 *
 * \note The returned value is guaranteed to be different from every other
 * value returned before. Same thing as ::new_tmp
 *
 * \warning Not thread safe.
 */
unsigned int new_label();
#endif // __INTERCODE_H__
