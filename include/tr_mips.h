#ifndef __TR_MIPS_H__
#define __TR_MIPS_H__

#include <stdio.h>

#include "intercode.h"
#include "program.h"

#define ISLOCAL 0
#define ISTEMP 1
#define ISGLOBAL 2

#define FILL_VAR(VAR, N)                                                       \
  do {                                                                         \
    int diff = (int)(N - s->offset);                                           \
    VAR.type =                                                                 \
        diff < 0 ? ISGLOBAL : (size_t)diff >= s->size ? ISTEMP : ISLOCAL;      \
    VAR.tmp = N;                                                               \
  } while (0)

#define PUSH(REG)                                                              \
  do {                                                                         \
    fprintf(out, "addiu $sp, $sp, -4\nsw %s, ($sp)\n", REG);                   \
  } while (0)

#define POP(REG)                                                               \
  do {                                                                         \
    fprintf(out, "lw %s, ($sp)\naddiu $sp, $sp, 4\n", REG);                    \
  } while (0)

#define STORE_LOCAL(REG, N)                                                    \
  do {                                                                         \
    Entry e = st_fetch_from_tmp(s->st, N.tmp);                                 \
    int offset = -4 * (s->stack_pos[N.tmp - s->offset] + 1);                   \
    if (e.by_ref) {                                                            \
      fprintf(out, "lw $t7, %d($fp)\n", offset);                               \
      fprintf(out, "sw %s, 0($t7)\n", REG);                                    \
    } else                                                                     \
      fprintf(out, "sw %s, %d($fp)\n", REG, offset);                           \
  } while (0)

#define STORE_GLOBAL(REG, N)                                                   \
  do {                                                                         \
    fprintf(out, "sw %s, _%s_\n", REG, get_global_name(gst, N.tmp));           \
  } while (0)

#define LOAD_LOCAL(REG, N)                                                     \
  do {                                                                         \
    Entry e = st_fetch_from_tmp(s->st, N.tmp);                                 \
    fprintf(out, "lw %s, %d($fp)\n", REG,                                      \
            -4 * (s->stack_pos[N.tmp - s->offset] + 1));                       \
    if (e.by_ref)                                                              \
      fprintf(out, "lw %s, (%s)\n", REG, REG);                                 \
  } while (0)

#define LOAD_GLOBAL(REG, N)                                                    \
  do {                                                                         \
    fprintf(out, "lw %s, _%s_\n", REG, get_global_name(gst, N.tmp));           \
  } while (0)

#define LOAD(REG, N)                                                           \
  do {                                                                         \
    switch (N.type) {                                                          \
    case ISTEMP:                                                               \
      POP(REG);                                                                \
      break;                                                                   \
    case ISGLOBAL:                                                             \
      LOAD_GLOBAL(REG, N);                                                     \
      break;                                                                   \
    case ISLOCAL:                                                              \
      LOAD_LOCAL(REG, N);                                                      \
      break;                                                                   \
    }                                                                          \
  } while (0)

#define LOAD_ADDR(REG, N)                                                      \
  do {                                                                         \
    switch (N.type) {                                                          \
    case ISGLOBAL:                                                             \
      fprintf(out, "la %s, _%s_\n", REG, get_global_name(gst, N.tmp));         \
      break;                                                                   \
    case ISLOCAL:                                                              \
      fprintf(out, "lw %s, %d($fp)\n", REG,                                    \
              -4 * (s->stack_pos[N.tmp - s->offset] + 1));                     \
      break;                                                                   \
    }                                                                          \
  } while (0)

#define LOAD_ARRAY(REG, N)                                                     \
  do {                                                                         \
    switch (N.type) {                                                          \
    case ISGLOBAL:                                                             \
      fprintf(out, "la %s, _%s_\n", REG, get_global_name(gst, N.tmp));         \
      break;                                                                   \
    case ISLOCAL: {                                                            \
      Entry e = st_fetch_from_tmp(s->st, N.tmp);                               \
      if (e.by_ref)                                                            \
        fprintf(out, "lw %s, %d($fp)\n", REG,                                  \
                -4 * (s->stack_pos[N.tmp - s->offset] + 1));                   \
      else {                                                                   \
        fprintf(out, "la %s, %d($fp)\n", REG,                                  \
                -4 * (s->stack_pos[N.tmp - s->offset] + 1));                   \
        fprintf(out, "addiu %s, %s, -%u\n", REG, REG,                          \
                get_total_array_size(e.arr) * 4 - 4);                          \
      }                                                                        \
    } break;                                                                   \
    default:                                                                   \
      break;                                                                   \
    }                                                                          \
  } while (0)

#define STORE(REG, N)                                                          \
  do {                                                                         \
    switch (N.type) {                                                          \
    case ISTEMP:                                                               \
      PUSH(REG);                                                               \
      break;                                                                   \
    case ISGLOBAL:                                                             \
      STORE_GLOBAL(REG, N);                                                    \
      break;                                                                   \
    case ISLOCAL:                                                              \
      STORE_LOCAL(REG, N);                                                     \
      break;                                                                   \
    }                                                                          \
  } while (0)

#define BINOP(INSTR)                                                           \
  do {                                                                         \
    LOAD("$t1", vy);                                                           \
    LOAD("$t0", vx);                                                           \
    fprintf(out, "%s $t2, $t0, $t1\n", INSTR);                                 \
    STORE("$t2", vres);                                                        \
  } while (0)

#define LOAD_IM(REG, VAL)                                                      \
  do {                                                                         \
    fprintf(out, "li %s, %d\n", REG, VAL);                                     \
  } while (0)

#define NEWLINE()                                                              \
  do {                                                                         \
    fprintf(out, "li $v0, 11\nli $a0, 10\nsyscall\n");                         \
  } while (0)

#define WRITE_INT()                                                            \
  do {                                                                         \
    fprintf(out, "li $v0, 1\nsyscall\n");                                      \
  } while (0)

#define WRITE_TRUE()                                                           \
  do {                                                                         \
    fprintf(out, "li $v0, 4\nla $a0, true\nsyscall\n");                        \
  } while (0)

#define WRITE_FALSE()                                                          \
  do {                                                                         \
    fprintf(out, "li $v0, 4\nla $a0, false\nsyscall\n");                       \
  } while (0)

#define CTX_STORE()                                                            \
  do {                                                                         \
    PUSH("$ra");                                                               \
    PUSH("$fp");                                                               \
  } while (0)

#define CTX_LOAD()                                                             \
  do {                                                                         \
    fprintf(out, "move $sp, $fp\n");                                           \
    POP("$fp");                                                                \
    POP("$ra");                                                                \
  } while (0)

#define MOVE_STACK(N)                                                          \
  do {                                                                         \
    Entry f = st_fetch_from_tmp(gst, N.tmp);                                   \
    Stack *s = stack_from_st(f.func.st);                                       \
    unsigned int no_vars = f.func.arity == s->size;                            \
    if (!no_vars)                                                              \
      fprintf(out, "addiu $sp, $sp -%d\n",                                     \
              (s->total_stack_size -                                           \
               (f.func.arity ? s->stack_pos[f.func.arity] : 0)) *              \
                  4);                                                          \
    stack_free(s);                                                             \
  } while (0)

#define COMPUTE_FP(N)                                                          \
  do {                                                                         \
    Entry f = st_fetch_from_tmp(gst, N.tmp);                                   \
    Stack *s = stack_from_st(f.func.st);                                       \
    fprintf(out, "addiu $fp, $sp %d\n", (s->total_stack_size) * 4);            \
    stack_free(s);                                                             \
  } while (0)

typedef struct {
  unsigned int *stack_pos;
  size_t size;
  int offset;
  SymTable *st;
  unsigned int total_stack_size;
} Stack;

Stack *stack_from_st(SymTable *);

const char *get_global_name(SymTable *, unsigned int);

int tr_prg(Program *, FILE *);

int tr_gst(SymTable *, FILE *);

int tr_gst_fun(SymTable *, FILE *);

int tr_main_fun(Program *, FILE *);

int tr_fun(struct func *, SymTable *, FILE *);

int tr_quad(Quad *, Stack *, SymTable *, FILE *);

Stack *stack_new();

void stack_free(Stack *);

#endif // __TR_MIPS_H__
