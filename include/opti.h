/*! \file opti.h
    \brief Optimization of the intermediate code
*/

#ifndef __OPTI_H__
#define __OPTI_H__

#include "cfg.h"
#include "program.h"

/*! \brief Run a round of optimization (CSE + copy propagation)
 *
 * \param program Pointer on the program in 3AC to optimize
 *
 * \returns A pointer on the same program, optimized.
 */
Program *optimization(Program *program, CFG *cfg);

#endif // __OPTI_H__