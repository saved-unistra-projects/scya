#ifndef __UTILS_H__
#define __UTILS_H__

#include "parser.h"
#include <string.h>

#define TEST_ERROR(EXP, OPERATOR, ANSWER)                                      \
  do                                                                           \
    if (EXP OPERATOR ANSWER) {                                                 \
      perror("Fatal error ");                                                  \
      exit(EXIT_FAILURE);                                                      \
    }                                                                          \
  while (0)

#define TEST_ERROR_W_MSG(EXP, OPERATOR, ANSWER, MESSAGE)                       \
  do                                                                           \
    if (EXP OPERATOR ANSWER) {                                                 \
      fprintf(stderr, "%s", MESSAGE);                                          \
      exit(EXIT_FAILURE);                                                      \
    }                                                                          \
  while (0)

inline void type2str(char *buf, int type) {
  switch (type) {
  case SCA_INT:
    strcpy(buf, "int");
    return;
  case SCA_BOOL:
    strcpy(buf, "bool");
    return;
  case SCA_UNIT:
    strcpy(buf, "unit");
    return;
  }
}

#endif // __UTILS_H__
