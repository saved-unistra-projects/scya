/*! \file symtable.h
    \brief Source file of symtable.c
*/

#ifndef __SYMTABLE_H__
#define __SYMTABLE_H__

#include "intercode.h"
#include <stdlib.h>

/*! \brief max size of a variable name
 */
#define MAX_STRING 128
#define MAX_SIZE 5

/*! \brief A structure representing the dimension of an array
 *
 *  Specify the sarting index and the size of the array
 */
typedef struct symtable SymTable;

struct dim {
  int start_index;
  unsigned long size;
};

/*! \brief A structure representing an entry in the symbol table
 *
 * Each symbol has a pointer on the next symbol. The name is the name of the
 * variable in the program, the type is it type in the parser and tmp is the
 * temporary variable associated with it.
 *
 * If the variable is an arrey, the struct arr is filed with the type of the
 * array, and dim and dims are the dimensions.
 */
struct arr {
  int type; // Atomic type of the array
  unsigned int dim;
  struct dim *dims;
};

struct parameter {
  int by_ref;
  char *name;
};

struct func {
  int ret_type;
  unsigned int arity;
  struct parameter *params;
  Quad *code;
  SymTable *st;
};

/* An entry in the symbol table */
typedef struct Entry {
  char *name;
  int type;         // Same as the types in parser.h
  unsigned int tmp; // Temporary variable associated with this entry
  // Should only be use when type == SCA_ARRAY
  union {
    struct arr arr;
    struct func func;
  };
  struct Entry *next;
  int by_ref;
} Entry;

/*! \brief A structure representing a symbole tablerepresenting
 *
 * Table is a  pointer on a hash table which contains the symbols and size is it
 * size.
 * Table 1D is a one-dimension table which contains the symbols and size1D is it
 * size.
 */
typedef struct symtable {
  Entry **table;
  int size;
  // 1D table
  Entry **table1D;
  size_t index;
  size_t size1D;
} SymTable;

/*! \brief Compute the index of a symbol with it name
 *
 * \param str the name of the symbol
 * \param size size of the hash table
 *
 *  \returns the index of the symbole in the hash table
 */
int hash(char *str, int size);

/*! \brief Create a new symbol table
 *
 * \returns a symbole table
 */
SymTable *st_new();

/*! \brief Add a new symbol in a symbol table
 *
 * \param Symtable* a pointer on a symbol table
 * \param char* the name of the symbol to add
 */
void st_add(SymTable *, char *);

/*! \brief Resize the hash table of the symbol table
 *
 * \param Symtable* the symbol table of the hash table we want to resize
 */
void resize_hash_table(SymTable *);

/*! \brief Set a given symbol at the place of another one
 *
 * \param Symtable* a symbol table
 * \param char* the  name of the symbol to change
 * \param Entry* the new symbol to place instead of the old one
 */
void st_set(SymTable *, char *, Entry);

/*! \brief Edit the tmp value of a given symbol
 *
 * \param Symtable* a symbol table
 * \param char* the name of the symbol
 * \param int the new tmp value
 */
void st_set_tmp(SymTable *, char *, unsigned int);

/*! \brief Search an symbol in the symbole table
 *
 * \param Symtable* a symbol table
 * \param char* the name of the symbol
 *
 * \returns the index the symbol in the 1D table
 */
size_t st_search(SymTable *, char *);

/*! \brief Add the correct type of the last added entries
 *
 * \param Symtable* a symbol table
 * \param int the type
 *
 * \returns 0
 */

void st_set_by_ref(SymTable *, char *, int);

int st_complete_type(SymTable *, int);

/*! \brief Add the correct type and dim of the last added arrays
 *
 * \param Symtable* a symbol table
 * \param int the type
 * \param the dim
 *
 * \returns 0
 */
int st_complete_array_type(SymTable *, int, unsigned int, struct dim *);
int st_complete_func_type(SymTable *st, int ret_type, unsigned int arity,
                          struct parameter *params, Quad *code, SymTable *lst);
void st_complete_code(SymTable *, char *, Quad *);

/*! \brief Search a symbol by it name
 *
 * \param Symtable* a symbol table
 * \param char* the name of the symbol
 *
 * \returns the symbol
 */
Entry st_fetch(SymTable *, char *);

/*! \brief Free a given symbole table
 *
 * \param Symtable* a symbol table
 */
void st_free(SymTable *);

/*! \brief Display a given symbol table on the terminal
 *
 * \param Symtable* a symbol table
 */
void st_print(SymTable *, int);
Entry st_fetch1D(SymTable *, int);
Entry st_fetch_from_tmp(SymTable *, unsigned int);

unsigned int get_total_array_size(struct arr);

void st_print_from_func(SymTable *, char *, int);

#endif // __SYMTABLE_H__
