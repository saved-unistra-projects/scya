/*! \file program.h
 * \brief Definitions for the program and function structures.
 *
 * This file defines what a program and a function is. Those structures
 * store symbol tables and code.
 */
#ifndef __PROGRAM_H__
#define __PROGRAM_H__

#include <stdlib.h>

#include "intercode.h"
#include "symtable.h"

//! Maximum number of function (temporary).
#define MAX_FUNC_SIZE 10

/*! \brief Represents a function.
 *
 * Not used yet (temporary).
 */
typedef struct {
  SymTable *st; //!< A pointer to a the local symbol table of the function.
  Quad *code; //!< A list of quadruplets (see ::Quad) that holds the code of the
              //!< function.
} Function;

/*! \brief Represents the program.
 *
 * This structure represents the whole program in 3AC (which will be translated
 * to machine code later). It contains a main function, a global symbol
 * table and a list of functions.
 */
typedef struct {
  Function **functions; //!< List of functions of the program.
  Quad *code; //!< A list of quadruplets (see ::Quad) that hold the code of the
              //!< main function.
  SymTable *gst;   //!< The global symbol table.
  size_t size;     //!< Real number of functions.
  size_t size_max; //!< Maximum size of the function list (used for resizing).
} Program;

/*! \brief Create a new program.
 *
 * \returns a pointer to the created program structure.
 * \note Returns `NULL` in case of failure.
 */
Program *prg_new();

/*! \brief Free a program.
 *
 * \param p a pointer to the program to free.
 *
 * \sidef Frees the program, its code and its functions.
 */
void prg_free(Program *p);

/*! \brief Set the code for the main function to the specified program.
 *
 * \param p a pointer to the target program.
 * \param l a list of quadruplets that hold the main function code to set.
 *
 * \sidef Set the code of \p p to \p l.
 */
void prg_set_code(Program *p, Quad *l);

/*! \brief Set the global symbol table to the specified program.
 *
 * \param p a pointer to the target program.
 * \param st a pointer to the symbol table to set.
 *
 * \sidef Set the global symbol table of \p p to \p st.
 */
void prg_set_gst(Program *p, SymTable *st);

/*! \brief Create a new function from a symbol table and a list of quadruplets.
 *
 * \param st a pointer to the local symbol table of the function to create.
 * \param l a list of quadruplets that hold the code of the function.
 *
 * \returns a pointer to the created function.
 * \note Returns `NULL` in case of failure.
 */
Function *fun_new(SymTable *st, Quad *l);

/*! \brief Free the target function.
 *
 * \param f a pointer to the function to free.
 *
 * \sidef Frees the passed function and its code and symbol table.
 */
void fun_free(Function *f);

/*! \brief Add a function to a program.
 *
 * \param p a pointer to the target function.
 * \param f a pointer to the function to add.
 *
 * \sidef Add the \p f to the function list of \p p.
 */
void fun_add(Program *p, Function *f);

#endif // __PROGRAM_H__
