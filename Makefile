COLOR_NUMBER = 36
# Directories
O_DIR = obj/
EXEC_DIR = bin/
SRC_DIR = src/
DIST_NAME = COLIN_DUHAMEL_REILAND_SCHIEBER
HEADERS_DIR = include/
DOC_DIR = doc/
TEST_DIR = tests/

# Special files
PARSER_H = $(HEADERS_DIR)parser.h

# Compilation
CC = cc
LEX = flex
YACC = yacc
override CFLAGS += -g -Wall -Wextra
override LFLAGS += -s

SOURCES = $(wildcard $(SRC_DIR)*.c)
OBJECTS = $(patsubst $(SRC_DIR)%.c, $(O_DIR)%.o, $(SOURCES))
TESTSRC = $(wildcard $(TEST_DIR)*.c)
TESTEXEC = $(patsubst $(TEST_DIR)%.c, $(TEST_DIR)%.test, $(TESTSRC))
TESTS = $(patsubst $(TEST_DIR)%.test, %.test, $(TESTEXEC))

# Executable name and main source file name
EXEC = scalpa
EXEC_NAME = scalpa


OBJECTSNOEXEC := $(filter-out $(O_DIR)$(EXEC).o, $(OBJECTS))


INIT_DIR = @mkdir -p

# Colors
COLOR = \033[1;$(COLOR_NUMBER)m
ENDCOLOR = \033[0m

vpath %.c src
vpath %.o obj
vpath %.h include
vpath main bin

# make all
all : $(EXEC_DIR)$(EXEC_NAME) $(TESTEXEC)

# make and run the program
run: $(EXEC_DIR)$(EXEC_NAME)
	@$(EXEC_DIR)$(EXEC_NAME)

install: $(EXEC_DIR)$(EXEC_NAME)
	@cp $< /usr/local/bin/

# check for memory leaks 
memcheck: $(EXEC_DIR)$(EXEC_NAME)

# Compile executable from objects
$(EXEC_DIR)$(EXEC_NAME) : $(OBJECTS) $(O_DIR)lexer.o $(O_DIR)parser.o $(PARSER_H)
	@echo "$(COLOR)------------------------Linking$(ENDCOLOR)"
	$(INIT_DIR) $(EXEC_DIR)
	$(CC) $(CFLAGS) -o $@ $^

# Compile main object
$(O_DIR)$(EXEC).o : $(SRC_DIR)$(EXEC).c $(PARSER_H)
	@echo "$(COLOR)------------------------Compiling $@$(ENDCOLOR)"
	$(INIT_DIR) $(EXEC_DIR)
	$(CC) $(CFLAGS) -I $(HEADERS_DIR) -c  $< -o $@

# Compile objects
$(O_DIR)%.o : $(SRC_DIR)%.c $(HEADERS_DIR)%.h $(PARSER_H)
	@echo "$(COLOR)------------------------Compiling $@$(ENDCOLOR)"
	$(INIT_DIR) $(O_DIR)
	$(CC) $(CFLAGS) -I $(HEADERS_DIR) -c $< -o $@

# Compile yacc file
$(O_DIR)parser.o : $(SRC_DIR)parser.y
	@echo "$(COLOR)------------------------Compiling parser $(ENDCOLOR)"
	$(YACC) $< --defines=$(PARSER_H) -o $(SRC_DIR)parser.c
	$(INIT_DIR) $(O_DIR)
	$(CC) -I $(HEADERS_DIR) $(CFLAGS) -c $(SRC_DIR)parser.c -o $@
	rm -f $(SRC_DIR)parser.c

$(PARSER_H) : $(O_DIR)parser.o

# Compile lex file
$(O_DIR)lexer.o : $(SRC_DIR)lexer.l $(HEADERS_DIR)* $(PARSER_H)
	@echo "$(COLOR)------------------------Compiling lexer $(ENDCOLOR)"
	$(LEX) $(FLEXOPT) $(LFLAGS) -o $(SRC_DIR)lexer.c $<
	$(CC) -I $(HEADERS_DIR) $(CFLAGS) -c $(SRC_DIR)lexer.c -o $@
	rm -f $(SRC_DIR)lexer.c

# Tests

# Run all tests
test: $(EXEC_DIR)$(EXEC_NAME) $(TEST_DIR)
	@./test.sh

# Run a specific test
%.test: $(TEST_DIR)%.test
	@echo "$(COLOR)------------------------Testing $@$(ENDCOLOR)"
	$(TEST_DIR)$@

# Compile tests
$(TEST_DIR)%.test: $(TEST_DIR)%.c $(OBJECTSNOEXEC)
	@echo "$(COLOR)------------------------Compiling test $@$(ENDCOLOR)"
	$(CC) $(CLFAGS) -I $(HEADERS_DIR) -I $(TEST_DIR) -o $@ $< $(OBJECTSNOEXEC)

# Clean and memcheck
memcheck : $(EXEC_DIR)$(EXEC_NAME)
	@echo "$(COLOR) ------------------------Stating memory leak check for $(EXEC)$(ENDCOLOR)"
	valgrind --leak-check=full $<


clean :
	@echo "$(COLOR)------------------------Cleaning$(ENDCOLOR)"
	rm -f $(OBJECTS) $(EXEC_DIR)$(EXEC_NAME) $(TEST_DIR)*.test \
		$(O_DIR)lexer.o $(SRC_DIR)lexer.c $(O_DIR)parser.o $(SRC_DIR)parser.c \
		$(HEADERS_DIR)parser.h

clean_doc :
	@echo "$(COLOR) -----------------------Cleaning doc$(ENDCOLOR)"
	rm -rf $(DOC_DIR)

doc : $(HEADERS_DIR)* Doxyfile
	@echo "$(COLOR) -----------------------Generating doc$(ENDCOLOR)"
	doxygen

dist :
	mkdir -p $(DIST_NAME)
	cp src/ include/ Makefile README.md Rapport.pdf $(DIST_NAME) -r  
	tar -c $(DIST_NAME) -f $(DIST_NAME).tar
	rm -rf $(DIST_NAME)
